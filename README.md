Attrition
=======

A browser based geopolitical simulation game.
-----------

Create and control your own fictional country. Establish a world power and keep the peace with other nations, or rule with an iron fist and become a harbinger of war. Decide your country's history, and the history of the game of Attrition.

Take your nation anywhere you go. Attrition is built upon the bootstrap framework for it's ease of use and responsive design to be played anywhere on almost any device.