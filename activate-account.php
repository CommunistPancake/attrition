<?php

	require_once("common/navigation.php");

	ob_start();
  	require_once("models/config.php");
  	if(isUserLoggedIn()) { header("Location: index.php"); die(); }

	$errors = array();

	//Get token param
	if(isset($_GET["token"])) {
			
			$token = $_GET["token"];
			
			if(!isset($token)) {

				$errors[] = lang("FORGOTPASS_INVALID_TOKEN");
			
			} else if(!validateactivationtoken($token)) { //Check for a valid token. Must exist and active must be = 0

				$errors[] = "Token does not exist / Account is already activated";
			
			} else {
				//Activate the users account
				if(!setUseractive($token)) {

					$errors[] = lang("SQL_ERROR");

				}

				require_once("common/db/link_mysql.php");

				$result = mysql_query("SELECT * FROM attr_users WHERE activationtoken='$token'", $link);
				if (!$result) {
					die('Could not query:' . mysql_error());
				} else {
					$new_a = mysql_fetch_array($result, MYSQL_BOTH);
				}

				// Event log
                $sql = "INSERT INTO {$dbprefix}events (event_type,event_action) VALUES (:event_type,:event_action)";
                $stmt = $pdo->prepare($sql);

                // Config
                $event_type = 'general';
                $event_action = '<a href="user.php?uid='.$new_a[user_id].'">'.stripcslashes(ucwords($new_a[country_name])).'</a> has been officially recognized as a country.';

                $stmt->bindParam(':event_type', $event_type, PDO::PARAM_STR);
                $stmt->bindParam(':event_action', $event_action, PDO::PARAM_STR);
                $stmt->execute();
			}
	
	} else {

		$errors[] = lang("FORGOTPASS_INVALID_TOKEN");

	}
?>

	<head>
		<title>Account Activation | <?php echo $websiteName; ?> </title>
	</head>

	<body>

  		<div class="container">
    		<div class="well">

	  			<? 
				  	if(count($errors) == 0) {

				   		$outcome_good[] = 'Activation Complete. You may now <a href="login">login.</a>';
					}
	  				require_once("common/alerts.php");
			   	?>

  				<? require_once("common/footer.php"); ?>
    		</div>
  		</div>
	</body>
</html>