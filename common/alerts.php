<html>
    <body>
        <div class="container-fluid">
            <center>
            <p class="text-muted">
        
            <?
                require_once("common/basicfunctions.php");
                require_once("common/userdata.php");

                if($u_a[donor] == false) {
                    $alerts = array(
                    '<span class="text-muted"><i class="fa fa-money"></i> Attrition was created with free access of all features to everyone. <a href="donate">Support the developers.</a></span>',
                    // '<i class="fa fa-euro"></i> If you don\'t <a href="donate">donate your money</a> the server could be "mysteriously hacked!"',
                    '<i class="fa fa-bug"></i> If you find a bug please use the <a href="https://bitbucket.org/sven62/attrition/issues" target="_blank">issue tracker</a> to report it.',
                    '<span class="text-muted">This release is incomplete and many features will be missing. Please use the <a href="https://bitbucket.org/sven62/attrition/issues" class="alert-link" target="_blank">issue tracker</a> if you find any bugs.</span>',
                    'Want a quote to be added to the footer? <a href="https://bitbucket.org/sven62/attrition/issue/1/footer-quote-request-thread" target="_blank">Request it here.</a>',
                    ''
                    );
                } else {
                    $alerts = array(
                        '<i class="fa fa-heart-o"></i> Thanks for being awesome ' . $u_a[username] . '.',
                        '<i class="fa fa-heart-o"></i> Thank you based ' . $u_a[username] . '.',
                        '<i class="fa fa-bug"></i> If you find a bug please use the <a href="https://bitbucket.org/sven62/bloc/issues" target="_blank">issue tracker</a> to report it.',
                        ''
                    );
                }

                if($alerts) {
                    $rand_keys = array_rand($alerts, 2);
                    echo alert(plain, $alerts[$rand_keys[0]]);
                }
            ?>

            </p>
          </center>
        </div>

        <?
            require_once("ungit/quickalerts.php");

            if(count($errors) > 0) {
                echo alert(danger, '<p>'.implode( "<p>" ,$errors));
            }
            if(count($message) > 0) {
                echo alert(success, '<p>'.implode( "<p>" ,$message));
            }

            if(count($outcome_bad) > 0) {
                echo alert(danger, '<p>'.implode( "<p>" ,$outcome_bad));
            }
            if(count($outcome_neutral) > 0) {
                echo alert(info, '<p>'.implode( "<p>" ,$outcome_neutral));
            }
            if(count($success) > 0) {
                echo alert(success, '<p>'.implode( "<p>" ,$success));
            }
            if(count($outcome_good) > 0) {
                echo alert(success, '<p>'.implode( "<p>" ,$outcome_good));
            }

            if(count($debug) > 0){
                echo debug(info, '<p>'.implode( "<p>" ,$debug));
            }
        ?>

        <hr>
    </body>
</html>