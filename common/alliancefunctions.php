<?
	if(!empty($_POST['alliance_new'])) {

		// Variables
		$cost = 100000;

		// Check that all forms are complete.
		if(empty($_POST['alliance_name'])) {
			$errors[] = 'You must enter an alliance name.';
		}
		if(empty($_POST['alliance_flag'])) {
			$errors[] = 'You must select an alliance flag.';
		}

		if(count($errors) == 0) {

			$debug_good[] = 'All forms completed.';

			// Validate
		    $alliance_name = sanitize(trim($_POST["alliance_name"]));

		    function alliance_nameExists($alliance_name)
		    {
		      
		      $sql = "SELECT alliance_name FROM {$dbprefix}alliances WHERE alliance_name='$alliance_name'";

		      if(returns_result($sql) > 0)
		        return true;
		      else
		        return false;
		    }

		    if(!$alliance_name) {
		      $errors[] = 'You must enter an alliance name.';
		    }
		    if(minMaxRange(3,20,$alliance_name))
		    {
		      $errors[] = lang("ACCOUNT_ALLIANCE_CHAR_LIMIT",array(3,20));
		    }
		    if(alliance_nameExists($alliance_name))
		    {
		      $errors[] = lang("ACCOUNT_ALLIANCE_IN_USE",array($alliance_name));
		    }

		   	// Validate variables
		    if($u_a[funds] < $cost) {
		    	$errors[] = 'You do not have enough funds to create an alliance.';
		    }

		    if(count($errors) == 0) {

		    	$debug_good[] = 'All data validated.';

				// Prepare the query
		      	$sql = "INSERT INTO {$dbprefix}alliances (alliance_id,founder_id,alliance_name,alliance_flag) VALUES (:alliance_id,:founder_id,:alliance_name,:alliance_flag)";
		      	$stmt = $pdo->prepare($sql);

		     	$result = mysql_query("SHOW TABLE STATUS LIKE '{$dbprefix}alliances'", $link);
		      	$a_a = mysql_fetch_assoc($result);

		      	$debug_info[] = 'Auto_increment: '.$a_a[Auto_increment];
		      	$debug_info[] = 'UID: '.$u_a[user_id];
		      	$debug_info[] = 'alliance_name: '.trim($_POST['alliance_name']);
		      	$debug_info[] = 'alliance_flag: '.trim($_POST['alliance_flag']);

		      	$stmt->bindParam(':alliance_id', $a_a[Auto_increment], PDO::PARAM_INT);
		      	$stmt->bindParam(':founder_id', $u_a[user_id], PDO::PARAM_INT);
		      	$stmt->bindParam(':alliance_name', trim($_POST['alliance_name']), PDO::PARAM_STR);
		      	$stmt->bindParam(':alliance_flag', trim($_POST['alliance_flag']), PDO::PARAM_STR);

		      	$stmt->execute();

			    if($stmt->rowCount() > 0) {

			    	$debug_good[] = 'Alliance data successfully processed.';
			    
			    } else {

			    	$debug_bad[] = 'No data entered.';
			    	$errors[] = 'Alliance creation failed. (No data entered)';

			    }

			    if(count($errors) == 0) {

			      	// Prepare the query
			      	$sql = ("UPDATE {$dbprefix}users SET funds = :funds, alliance_id = :alliance_id WHERE user_id='$u_a[user_id]'");
			      	$stmt = $pdo->prepare($sql);

			      	$funds = $u_a[funds] - $cost;

			      	// Bind
			      	$stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
			      	$stmt->bindParam(':alliance_id', $a_a[Auto_increment], PDO::PARAM_INT);

			      	$stmt->execute();

					// Calculate and set the number of members
			        $result = mysql_query("SELECT alliance_id FROM {$dbprefix}users WHERE alliance_id='$mail_a[var1]'", $link);
					if (!$result) {
						die('Could not query:' . mysql_error());
					} else {
			        	$rows = mysql_num_rows($result);
					    // Set the number of members
					    $sql = ("UPDATE {$dbprefix}alliances SET members = :members WHERE alliance_id='$mail_a[var1]'");
					    $stmt = $pdo->prepare($sql);
					    $stmt->bindParam(':members', $rows, PDO::PARAM_STR);
						$stmt->execute();
				    }

				    if($stmt->rowCount() > 0) {

				    	$debug_good[] = 'User data successfully processed.';
				    	$outcome_good[] = 'Alliance created successfully';
				    
				    } else {

				    	$debug_bad[] = 'No data entered.';
				    	$errors[] = 'User update failed. (No data entered)';

				    }

		    	}

		    }

		}

	}
	if (!empty($_POST['alliance_name']) and $u_a[user_id] == $a_a[founder_id] and $u_a[alliance_id] == $a_a[alliance_id]) {

	    // Prepare the query
	    $sql = ("UPDATE {$dbprefix}alliances SET alliance_name = :alliance_name WHERE founder_id='$u_a[user_id]'");
	    $stmt = $pdo->prepare($sql);

	    // Validate
	    $alliance_name = trim($_POST["alliance_name"]);

	    function alliance_nameExists($alliance_name)
	    {
	      
	      $sql = "SELECT alliance_name FROM {$dbprefix}alliances WHERE alliance_name='$alliance_name'";

	      if(returns_result($sql) > 0)
	        return true;
	      else
	        return false;
	    }

	    if(!$alliance_name) {
	      $errors[] = 'You must enter an alliance name.';
	    }
	    if(minMaxRange(3,35,$alliance_name))
	    {
	      $errors[] = 'You must enter an alliance name between 3, and 35 characters.';
	    }
	    if(alliance_nameExists($alliance_name))
	    {
	      $errors[] = 'That alliance name is in use.';
	    }

	    // Bind
	    $stmt->bindParam(':alliance_name', $_POST['alliance_name'], PDO::PARAM_STR);

	    // Continue without errors
	    if(count($errors) == 0) {
	      
	      $stmt->execute();
	      $outcome_good[] = 'Alliance name successfully updated.';
	    
	    }
		 if($u_a[user_id] != $a_a[founder_id]) {
			$errors[] = 'You are not the founder of this alliance.';
		} elseif ($u_a[alliance_id] != $a_a[alliance_id]) {
			$errors[] = 'You are not in this alliance.';

		}
	}
	if (!empty($_POST['alliance_invite']) and $u_a[user_id] == $a_a[founder_id] and $u_a[alliance_id] == $a_a[alliance_id]) {

        // Validate
        $alliance_invite = trim($_POST["alliance_invite"]);
        $alliance_invite = filter_var($alliance_invite, FILTER_SANITIZE_URL);

        $sql = mysql_query("SELECT user_id FROM {$dbprefix}users WHERE user_id='$alliance_invite' AND active='1' LIMIT 1", $link);
        if (!$sql) {
            $errors[] = 'A player or country with that id was not found.';
        } else {
		    $uid = $alliance_invite;
		    require("common/uiddata.php");
		    require("common/userdata.php");
        }

        $result = mysql_query("SELECT alliance_id FROM {$dbprefix}users WHERE user_id='$uid_a[user_id]' LIMIT 1", $link);
		if (!$result) {
			die('Could not query:' . mysql_error());
		} else {
        	$sql_a = mysql_fetch_array($result, MYSQL_BOTH);
	        if ($sql_a[alliance_id] != null) {
	            $errors[] = 'That country is already in an alliance.';
	        }
	    }
	    $result = mysql_query("SELECT * FROM {$dbprefix}mail WHERE (user_id='$alliance_invite') AND (var1='$a_a[alliance_id]') LIMIT 1", $link);
	    $sql_a = mysql_fetch_array($result, MYSQL_BOTH);
	    if($sql_a[var1] == $a_a[alliance_id]) {
	    	$errors[] = 'You have already sent that country an invite.';
	    }
	    if(!$alliance_invite) {
	      $errors[] = 'You must enter a user\'s id. (Found on their profile)';
	    }
	    if($uid_a[alliance_id] == $a_a[alliance_id]) {
	    	$errors[] = 'That country is already in the alliance.';
	    }

	    // Continue without errors
	    if(count($errors) == 0) {
		    $sql = ("INSERT INTO attr_mail (user_id, sender_id, mail_type, title, string, var1) VALUES(:user_id, :sender_id, :mail_type, :title, :string, :var1)");
		    $stmt = $pdo->prepare($sql);

		    $mail_type = 'invite';
		    $title = 'You have been invited to the alliance: '.stripcslashes(ucwords($a_a[alliance_name])).'.';
		    $string = '<a href="alliances?tab=confirm">Accept</a> or <a href="alliances?tab=confirm">Deny</a> the invitation.';
		    $var1 = $a_a[alliance_id];

		    $stmt->bindParam(':user_id', $uid_a[user_id], PDO::PARAM_INT);
		    $stmt->bindParam(':sender_id', $u_a[user_id], PDO::PARAM_INT);
		    $stmt->bindParam(':mail_type', $mail_type, PDO::PARAM_STR);
		    $stmt->bindParam(':title', $title, PDO::PARAM_STR);
		    $stmt->bindParam(':string', $string, PDO::PARAM_STR);
		    $stmt->bindParam(':var1', $var1, PDO::PARAM_INT);

		    if(count($errors) == 0) {
		    	$stmt->execute();

	            if($stmt->rowCount() > 0) {
	                $debug_good[] = 'Alliance invite private_message message successfully processed.';
	                $outcome_good[] = 'The alliance invite has been successfully sent.';

                    $sql = "INSERT INTO {$dbprefix}events (event_type,event_action,attacker_id) VALUES (:event_type,:event_action,:attacker_id)";
                    $stmt = $pdo->prepare($sql);

                    // Config
                    $event_type = 'invite';
                    $event_action = 'Has invited <a href="user.php?uid='.$uid_a[user_id].'">'.stripcslashes(ucwords($uid_a[country_name])).'</a> to <a href="alliance.php?aid='.$a_a[alliance_id].'">'.stripcslashes(ucwords($a_a[alliance_name])).'</a>';

                    $stmt->bindParam(':event_type', $event_type, PDO::PARAM_STR);
                    $stmt->bindParam(':event_action', $event_action, PDO::PARAM_STR);
                    $stmt->bindParam(':attacker_id', $u_a[user_id], PDO::PARAM_INT);
                    // $stmt->bindParam(':defender_id', $uid_a[user_id], PDO::PARAM_INT);
                    $stmt->execute();
	            } else {
	                $debug_bad[] = 'No alliance private_message data entered.';
	                $errors[] = 'Alliance invite private_message creation failed. (No data entered, Report this as a bug.)';
	            }
		    }

	    }
	}
	if(isset($_POST['accept_inv'])) {
		if($u_a[alliance_id] != null) {
			$errors[] = 'You are already in an alliance.';
		} else {
	        $result = mysql_query("SELECT * FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' LIMIT 1", $link);
			if (!$result) {
				die('Could not query:' . mysql_error());
			} else {
	        	$mail_a = mysql_fetch_array($result, MYSQL_BOTH);
		    }
		    $sql = ("UPDATE {$dbprefix}users SET alliance_id = :alliance_id WHERE user_id='$u_a[user_id]'");
		    $stmt = $pdo->prepare($sql);
		    $stmt->bindParam(':alliance_id', $mail_a[var1], PDO::PARAM_STR);
			$stmt->execute();

			// Calculate and set the number of members
	        $result = mysql_query("SELECT alliance_id FROM {$dbprefix}users WHERE alliance_id='$mail_a[var1]'", $link);
			if (!$result) {
				die('Could not query:' . mysql_error());
			} else {
	        	$rows = mysql_num_rows($result);
			    // Set the number of members
			    $sql = ("UPDATE {$dbprefix}alliances SET members = :members WHERE alliance_id='$mail_a[var1]'");
			    $stmt = $pdo->prepare($sql);
			    $stmt->bindParam(':members', $rows, PDO::PARAM_STR);
				$stmt->execute();
		    }

		    if(count($errors) == 0) {
			 //    $result = mysql_query("SELECT * FROM {$dbprefix}alliances WHERE alliance_id='$mail_a[var1]' LIMIT 1", $link);
				// if (!$result) {
				// 	die('Could not query:' . mysql_error());
				// } else {
				// 	$result = mysql_fetch_assoc($result);
				// }
				require("userdata.php");
				$aid = $u_a[alliance_id];
				require("alliancedata.php");

	            $sql = "INSERT INTO {$dbprefix}events (event_type,event_action,attacker_id) VALUES (:event_type,:event_action,:attacker_id)";
	            $stmt = $pdo->prepare($sql);
	            $event_type = 'alliance';
	            $event_action = 'has joined <a href="alliance.php?aid='.$a_a[alliance_id].'">'.stripcslashes(ucwords($a_a[alliance_name])).'</a>';
	            $stmt->bindParam(':event_type', $event_type, PDO::PARAM_STR);
	            $stmt->bindParam(':event_action', $event_action, PDO::PARAM_STR);
	            $stmt->bindParam(':attacker_id', $u_a[user_id], PDO::PARAM_INT);
	            $stmt->execute();

      			$sql = "DELETE FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' AND mail_type='invite' AND var1='$mail_a[var1]' LIMIT 1";
      			$stmt = $pdo->prepare($sql);
      			$stmt->execute();

		      	// Continue without errors
		      	if(count($errors) == 0) { 
		        	$outcome_good[] = 'Invite successfully accepted.';   
		      	}
		    }
		}
	}
	if(isset($_POST['deny_inv'])) {
	    $result = mysql_query("SELECT var1 FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' ORDER BY mail_date DESC LIMIT 1", $link);
		if (!$result) {
			die('Could not query:' . mysql_error());
		} else {
	    	$mail_a = mysql_fetch_array($result, MYSQL_BOTH);
	    }

      	$sql = "DELETE FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' AND mail_type='invite' AND var1='$mail_a[var1]' LIMIT 1";
      	$stmt = $pdo->prepare($sql);

      	// Continue without errors
      	if(count($errors) == 0) {
        	$stmt->execute();

        	if($stmt->rowCount() > 0) {
        		$outcome_good[] = 'Invite successfully denied.';
        	}
      	}
	}
	if (!empty($_POST['alliance_kick']) and $u_a[user_id] == $a_a[founder_id] and $u_a[alliance_id] == $a_a[alliance_id]) {
        $kick_id = trim($_POST['alliance_kick']);
        $kick_id = filter_var($kick_id, FILTER_SANITIZE_URL);

        $result = mysql_query("SELECT * FROM {$dbprefix}users WHERE user_id='$kick_id' AND active='1' LIMIT 1", $link);
		if (!$result) {
			die('Could not query:' . mysql_error());
		} else {
        	$sql_a = mysql_fetch_array($result, MYSQL_BOTH);
	    }
        if($kick_id == $u_a[user_id]) {
        	$errors[] = 'You cannot kick yourself from the Alliance.';
        }
        if($sql_a[alliance_id] != $a_a[alliance_id]) {
        	$errors[] = 'That user is not in the alliance';
        }
        if(!$errors) {
	    	$sql = ("UPDATE {$dbprefix}users SET alliance_id = :alliance_id WHERE user_id='$sql_a[user_id]'");
	    	$stmt = $pdo->prepare($sql);
	    	$stmt->bindParam(':alliance_id', $n = null, PDO::PARAM_NULL);
	      	$stmt->execute();
	      	$outcome_good[] = 'User successfully kicked from the alliance.';

			// Calculate and set the number of members
	        $result = mysql_query("SELECT alliance_id FROM {$dbprefix}users WHERE alliance_id='$mail_a[var1]'", $link);
			if (!$result) {
				die('Could not query:' . mysql_error());
			} else {
	        	$rows = mysql_num_rows($result);
			    // Set the number of members
			    $sql = ("UPDATE {$dbprefix}alliances SET members = :members WHERE alliance_id='$mail_a[var1]'");
			    $stmt = $pdo->prepare($sql);
			    $stmt->bindParam(':members', $rows, PDO::PARAM_STR);
				$stmt->execute();
		    }

            $sql = "INSERT INTO {$dbprefix}events (event_type,event_action,attacker_id) VALUES (:event_type,:event_action,:attacker_id)";
            $stmt = $pdo->prepare($sql);
            $event_type = 'kicked';
            $event_action = 'has kicked <a href="user.php?uid='.$sql_a[user_id].'">'.stripcslashes(ucwords($sql_a[country_name])).'</a> from <a href="alliance.php?aid='.$a_a[alliance_id].'">'.stripcslashes(ucwords($a_a[alliance_name])).'</a>';
            $stmt->bindParam(':event_type', $event_type, PDO::PARAM_STR);
            $stmt->bindParam(':event_action', $event_action, PDO::PARAM_STR);
            $stmt->bindParam(':attacker_id', $u_a[user_id], PDO::PARAM_INT);
            $stmt->execute();
        }
	}
	if(isset($_POST['leave_alliance'])) {
	    $result = mysql_query("SELECT * FROM {$dbprefix}alliances WHERE founder_id='$u_a[user_id]' LIMIT 1", $link);
		if (!$result) {
			die('Could not query:' . mysql_error());
		}
		if($u_a[alliance_id] == null) {
			$errors[] = 'You are not in an alliance.';
		}

		$result = mysql_query("SELECT user_id FROM {$dbprefix}users WHERE alliance_id='$u_a[alliance_id]'", $link);
		$members = mysql_fetch_assoc($result);
		$total_members = mysql_num_rows($result);

		if($total_members <= 1) {
			$sql = ("DELETE FROM {$dbprefix}alliances WHERE alliance_id = :alliance_id");
			$stmt = $pdo->prepare($sql);

			$stmt->bindParam(':alliance_id', $u_a[alliance_id], PDO::PARAM_INT); 
			$stmt->execute();

			$outcome_good[] = 'The alliance was empty and has been deleted.';
		}

		if(!$errors) {
	        $sql = ("UPDATE {$dbprefix}users SET alliance_id = :alliance_id WHERE user_id='$u_a[user_id]'");
	        $stmt = $pdo->prepare($sql);

	        $alliance_id = null;

	        $stmt->bindParam(':alliance_id', $alliance_id, PDO::PARAM_INT);
	        $stmt->execute();
	        $outcome_good[] = 'You have left the alliance.';

			// Calculate and set the number of members
	        $result = mysql_query("SELECT alliance_id FROM {$dbprefix}users WHERE alliance_id='$mail_a[var1]'", $link);
			if (!$result) {
				die('Could not query:' . mysql_error());
			} else {
	        	$rows = mysql_num_rows($result);
			    // Set the number of members
			    $sql = ("UPDATE {$dbprefix}alliances SET members = :members WHERE alliance_id='$mail_a[var1]'");
			    $stmt = $pdo->prepare($sql);
			    $stmt->bindParam(':members', $rows, PDO::PARAM_STR);
				$stmt->execute();
		    }

            $sql = "INSERT INTO {$dbprefix}events (event_type,event_action,attacker_id) VALUES (:event_type,:event_action,:attacker_id)";
            $stmt = $pdo->prepare($sql);
            $event_type = 'left_alliance';
            $event_action = 'has left <a href="alliance.php?aid='.$a_a[alliance_id].'">'.stripcslashes(ucwords($a_a[alliance_name])).'</a>';
            $stmt->bindParam(':event_type', $event_type, PDO::PARAM_STR);
            $stmt->bindParam(':event_action', $event_action, PDO::PARAM_STR);
            $stmt->bindParam(':attacker_id', $u_a[user_id], PDO::PARAM_INT);
            $stmt->execute();
		}
	}
	if(!empty($_POST['alliance_description'])) {
	    // Prepare the query
	    $sql = ("UPDATE {$dbprefix}alliances SET description = :description WHERE founder_id='$u_a[user_id]'");
	    $stmt = $pdo->prepare($sql);

	    $description = sanitize(trim($_POST["alliance_description"]));
	    if(!$description) {
	      $errors[] = 'You must enter an alliance name.';
	    }

	    // Bind
	    $stmt->bindParam(':description', $description, PDO::PARAM_STR);

	    // Continue without errors
	    if(count($errors) == 0) {
	      
	      $stmt->execute();
	      $outcome_good[] = 'Alliance description successfully updated.';
	    
	    }
	}
    if (!empty($_POST['anthem_url'])) {
        $sql = ("UPDATE {$dbprefix}alliances SET alliance_anthem = :alliance_anthem WHERE founder_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        // Validate
        $anthem_url = trim($_POST["anthem_url"]);
        $andthem_url = filter_var($anthem_url, FILTER_SANITIZE_URL);

        // Bind
        $stmt->bindParam(':alliance_anthem', $anthem_url, PDO::PARAM_STR);

        if(!$anthem_url) {
            $errors = 'You must enter an anthem url.';
        }
        if(minMaxRange(3,150,$anthem_url)) {
            $errors[] = 'The anthem url code must be between 3 and 150 characters.';
        }
        if(count($errors) == 0) {
            $stmt->execute();
            $success[] = 'Alliance anthem successfully updated.';
        }
    }
    if (!empty($_POST['alliance_flag'])) {
        // Validate
        $alliance_flag = sanitize(trim($_POST["alliance_flag"]));

        if(!$alliance_flag) {
          $errors[] = lang("ACCOUNT_COUNTRY_NO_FLAG");
        }
        if(count($errors) == 0) {
        	$sql = ("UPDATE {$dbprefix}alliances SET alliance_flag = :alliance_flag WHERE founder_id='$u_a[user_id]'");
        	$stmt = $pdo->prepare($sql);

        	// Bind
       		$stmt->bindParam(':alliance_flag', $alliance_flag, PDO::PARAM_STR);

            $stmt->execute();
            $success[] = 'Flag successfully updated.';

            $sql = ("UPDATE {$dbprefix}alliances SET custom_flag = :custom_flag WHERE founder_id='$u_a[user_id]'");
            $stmt = $pdo->prepare($sql);
            $custom_flag = null;
            $stmt->bindParam(':custom_flag', $custom_flag, PDO::PARAM_STR);
            $stmt->execute();
        }
    }
    if (!empty($_POST['custom_flag'])) {
        // if($u_a[donor] != true) {
        //     $errors[] = 'You must be a donor to set a custom flag';
        // }
        if(count($errors) == 0) {
            $custom_flag = sanitize(trim($_POST["custom_flag"]));

            if(!$custom_flag) {
              $errors[] = lang("ACCOUNT_COUNTRY_NO_FLAG");
            }
            if(count($errors) == 0) { 
            	$sql = ("UPDATE {$dbprefix}alliances SET custom_flag = :custom_flag WHERE founder_id='$u_a[user_id]'");
            	$stmt = $pdo->prepare($sql);

            	// Bind
            	$stmt->bindParam(':custom_flag', $_POST['custom_flag'], PDO::PARAM_STR);

                $stmt->execute();
                $success[] = 'Custom flag successfully updated.'; 

                $sql = ("UPDATE {$dbprefix}alliances SET alliance_flag = :alliance_flag WHERE founder_id='$u_a[user_id]'");
                $stmt = $pdo->prepare($sql);
                $alliance_flag = null;
                $stmt->bindParam(':alliance_flag', $alliance_flag, PDO::PARAM_STR);
                $stmt->execute();
            }
        }
    }
    if(isset($_POST['supply_troops'])) {
    	require_once("common/userdata.php");
    	$aid = mysql_real_escape_string($_GET["aid"]);
    	require_once("common/alliancedata.php");
        if($u_a[troops] < 10000) {
            $errors[] = 'You must have at least 10,000 troops to send.';
        }
        if(!$errors) {
            $sql = ("UPDATE {$dbprefix}users SET troops = :troops WHERE user_id='$u_a[user_id]'");
            $stmt = $pdo->prepare($sql);

            $troops = $u_a[troops] - 10000;

            $stmt->bindParam(':troops', $troops, PDO::PARAM_INT);
            $stmt->execute();

            $sql = ("UPDATE {$dbprefix}alliances SET troop_bank = :troop_bank WHERE alliance_id='$u_a[alliance_id]'");
            $stmt = $pdo->prepare($sql);

            $troops = $a_a[troop_bank] + 10000;

            $stmt->bindParam(':troop_bank', $troops, PDO::PARAM_INT);
            $stmt->execute();
            $outcome_good[] = 'You have successfully donated 10,000 troops.';

            $sql = "INSERT INTO {$dbprefix}events (event_type,event_action,attacker_id) VALUES (:event_type,:event_action,:attacker_id)";
            $stmt = $pdo->prepare($sql);
            $event_type = 'supply_troops';
            $event_action = 'has supplied 10,000 troops to <a href="alliance.php?aid='.$a_a[alliance_id].'">'.stripcslashes(ucwords($a_a[alliance_name])).'</a>';
            $stmt->bindParam(':event_type', $event_type, PDO::PARAM_STR);
            $stmt->bindParam(':event_action', $event_action, PDO::PARAM_STR);
            $stmt->bindParam(':attacker_id', $u_a[user_id], PDO::PARAM_INT);
            $stmt->execute();
        }
    }
?>