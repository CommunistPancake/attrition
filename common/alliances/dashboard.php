
<div class="row">
    <div class="col-md-4">

        <div class="list-group">

            <?php

                $use = 'u_a';
                $get = 'a_a';
                require_once("db/common/data_array.php");

                if(isUserLoggedIn() and $u_a[alliance_id]) {

                    echo '<a href="?tab=dashboard" class="list-group-item active">Dashboard</a>';

                }

                if($u_a[alliance_id] and $u_a[user_id] == $a_a[founder_id]) {

                    echo '<a href="?tab=preferences" class="list-group-item">Alliance Preferences</a>';

                }

            ?>

            <a href="?tab=list" class="list-group-item">Alliance Listing</a>

            <?php  

                if(isUserLoggedIn() and $u_a[alliance_id]) {

                    echo '<a href="alliance?uid=' . $u_a[alliance_id] . '" class="list-group-item">Alliance Profile: '.$a_a[alliance_name].'</a>';

                } elseif(isUserLoggedIn() and !$u_a[alliance_id]) {

                    echo '<a href="?tab=create" class="list-group-item">Create an Alliance</a>';
                
                }

            ?>

        </div>

    </div>

    <div class="col-md-8">

        <!-- Economic -->
       <div class="panel panel-success">

            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="icon-globe"></i> Alliance Summary
                </h3>
            </div>

            <div class="panel-body">

                      <?php

                        if(isUserLoggedIn() and $u_a[alliance_id]) {

                            // Founder name
                            $result = mysql_query("SELECT username FROM attr_users WHERE user_id='$a_a[founder_id]' LIMIT 1", $link);
                            $founder = mysql_fetch_array($result, MYSQL_BOTH);

                            // Member count
                            $result = mysql_query("SELECT alliance_id FROM attr_users WHERE alliance_id='$a_a[alliance_id]'", $link);
                            $members = mysql_num_rows($result);

                            // Total Territory
                            $result = mysql_query("SELECT territory FROM attr_users WHERE alliance_id='$a_a[alliance_id]'", $link);
                            $territory= 0;
                            while ($num = mysql_fetch_assoc ($result)) {

                                $territory += $num['territory'];

                            }

                            // Total Population
                            $result = mysql_query("SELECT population FROM attr_users WHERE alliance_id='$a_a[alliance_id]'", $link);
                            $population= 0;
                            while ($num = mysql_fetch_assoc ($result)) {

                                $population += $num['population'];

                            }

                            // Total Funds
                            $result = mysql_query("SELECT funds FROM attr_users WHERE alliance_id='$a_a[alliance_id]'", $link);
                            $funds= 0;
                            while ($num = mysql_fetch_assoc ($result)) {

                                $funds += $num['funds'];

                            }

                            // Total GDP
                            $result = mysql_query("SELECT gdp FROM attr_users WHERE alliance_id='$a_a[alliance_id]'", $link);
                            $gdp= 0;
                            while ($num = mysql_fetch_assoc ($result)) {

                                $gdp += $num['gdp'];

                            }

                            // Total Troops
                            $result = mysql_query("SELECT troops FROM attr_users WHERE alliance_id='$a_a[alliance_id]'", $link);
                            $troops= 0;
                            while ($num = mysql_fetch_assoc ($result)) {

                                $troops += $num['troops'];

                            }

                            echo '
                                <table class="table table-hover table-striped">
                            
                                    <thead>
                                      <tr>
                                        <th>Class</th>
                                        <th>Value</th>
                                      </tr>
                                    </thead>

                                <tbody>
                            ';

                            echo 
                            '<tr><td>Alliance</td><td><a href="?tab=profile?id=' . $a_a[alliance_id] . '">' . $a_a[alliance_name] . '</a></td></tr>';

                            echo
                            '<tr><td>Founder</td><td><a href="user.php?uid='.
                            $founder[username].
                            '">'.
                            ucfirst($founder[username]).
                            '</a></td></tr><tr><td>Total Members</td><td>'.
                            $english_format_number = number_format($members).
                            '</td></tr><tr><td>Total Territory</td><td>'.
                            $english_format_number = number_format($territory).
                            ' km<sup>2</sup></td></tr><tr><td>Total Population</td><td>'.
                            $english_format_number = number_format($population).
                            '</td></tr><tr><td>Total Funds</td><td>$'.
                            $english_format_number = number_format($funds).
                            '</td></tr><tr><td>Total GDP</td><td>$'.
                            $english_format_number = number_format($gdp).
                            '</td></tr><tr><td>Total Troops</td><td>'.
                            $english_format_number = number_format($troops)
                            ;

                            echo '</tbody></table>';

                        } elseif(isUserLoggedIn() and !$u_a[alliance_id]) {

                            echo 'You are not in an alliance.';

                        } elseif(!isUserLoggedIn()) {

                            echo 'You must be logged in to see your alliance summary.';

                        }

                      ?>
              
            </div>
        </div>

    </div>
</div>