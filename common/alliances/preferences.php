<div class="row">
  <? require_once("common/alliances/sidebar.php"); ?>

  <div class="col-md-8">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="icon-globe"></i> Alliance Preferences</h3>
      </div>
      <div class="panel-body">
        <? if($a_a[founder_id] == $u_a[user_id]) { ?>

          <div class="row">
              <div class="col-md-6">
                <form action="<? echo $_SERVER['PHP_SELF'] ?>?tab=preferences" method="post">
                <p>
                  <input type="text" class="form-control" placeholder="Alliance Name" name="alliance_name" maxlength="20">
                </p>
              </div>
              <div class="col-md-6">
                <p class="text-muted">
                  <? echo $a_a[alliance_name]; ?>
                </p>
              </div>
            </div>

            <hr><div class="row">
              <div class="col-md-6">
                <p>
                  <input type="text" class="form-control" placeholder="Invite A Country (UID)" name="alliance_invite" maxlength="11">
                </p>
              </div>
            </div>

            <hr><div class="row">
              <div class="col-md-6">
                <p>
                  <input type="text" class="form-control" placeholder="Kick from Alliance (UID)" name="alliance_kick" maxlength="11">
                </p>
              </div>
            </div>

            <hr><div class="row">
                <div class="col-md-6">
                    <p><textarea class="form-control" placeholder="Alliance Description" name="alliance_description" maxlength="150"></textarea></p>
                </div>
                <div class="col-md-6">
                    <?
                        if($u_a[description] == null) {
                            if($u_a[user_id] == $u_a[user_id]) {
                                echo '<p class="text-muted">Welcome to Attrition. This is the default country description. To change your description visit the <a href="preferences.php">settings</a> page.</p>';
                            } else {
                                echo '<p class="text-muted">This country\'s leader has not set their description yet. <em>The country might be an easy target for an attack.</em></p>';
                            }
                        } else {
                            echo '<p class="text-muted">';
                            if($a_a[description] != null) {
                              echo stripcslashes(ucfirst($a_a[description]));
                            } else {
                              echo 'No description set.';
                            }
                            echo '</p>';
                        }
                    ?>
                </div>
            </div>

            <hr><div class="row">
                <div class="col-md-6">
                    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                        <p><input type="text" class="form-control" placeholder="Alliance Anthem URL" name="anthem_url" maxlength="20"></p>
                </div>
                <div class="col-md-6">
                    <?
                        if($a_a[alliance_anthem]) {
                           echo '<p class="text-muted">Current URL: <a href="https://www.youtube.com/watch?v=' . $a_a[alliance_anthem] . '" target="_blank">'. $a_a[alliance_anthem] .'</a></p>';
                        } else {
                            echo '<p class="text-muted">Current URL: Not set.</p>';
                        }
                    ?>
                    <p class="text-muted"><small>This will only work with a video id from youtube. For example: <a href="https://www.youtube.com/watch?v=c4wHJqqud3U" target="_blank">c4wHJqqud3U</a></small></p>
                </div>
            </div>

             <hr><div class="row">
                  <div class="col-md-6">
                      <select class="form-control" name="alliance_flag">
                          <option value="" disabled selected style='display:none;'>Alliance Flag</option>
                          <? require_once("common/flagoptions.php"); ?>
                      </select>
                  </div>
                  <div class="col-md-6">
                      <center>                  
                          <?php if($a_a[alliance_flag] != null) { echo getflagfile($a_a[alliance_flag]); } ?>
                      </center>
                </div>
              </div>

              <hr><div class="row">
                  <div class="col-md-6">
                      <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                          <p><input type="text" class="form-control" placeholder="Custom Flag URL" name="custom_flag" maxlength="150"></p>
                  </div>
                  <div class="col-md-6">
                      <?
                          if($a_a[custom_flag]) {
                             echo '<img style="width: 100%;" src="'.$a_a[custom_flag].'"><p class="text-muted">Current URL: <a href="' . $u_a[custom_flag] . '" target="_blank">'. $a_a[custom_flag] .'</a></p>';
                          } else {
                              echo '<p class="text-muted">Current URL: Not set.</p>';
                          }
                      ?>
                      <p class="text-muted"><small>Make sure to include the entire url.</small></p>
                  </div>
              </div>

            <hr><div class="row">
              <div class="col-md-12">
                <p>
                  <input type="submit" class="btn btn-primary btn-block" value="Update Preferences" />
                </p>
                </form>
              </div>
            </div>

            <!-- Input length -->
            <script type="text/javascript">
              $('input.form-control').maxlength({
                    alwaysShow: true,
                    // threshold: 10,
                    warningClass: "label label-info",
                    limitReachedClass: "label label-s"
                });
            </script>
        <?
          } else {
              echo 'Only the alliance founder can update preferences for the alliance.';
          }
          if(!$u_a[alliance_id]) {
              echo 'You are not in an alliance.';
          }
          if(!isUserLoggedIn()) { 
              echo 'You must be logged in to change alliance preferences.';
          }
        ?>
      </div>
    </div>
  </div>
</div>