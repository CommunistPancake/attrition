<div class="col-md-4">

    <div class="list-group">

        <?php
            // if(isUserLoggedIn() and $u_a[alliance_id]) {
            //     echo '<a href="?tab=dashboard" class="list-group-item">Dashboard</a>';
            // }
            if($u_a[alliance_id] and $u_a[user_id] == $a_a[founder_id]) {
                echo '<a href="?tab=preferences" class="list-group-item">Alliance Preferences</a>';
            }
        ?>

        <a href="?tab=list" class="list-group-item">Alliance Listing</a>

        <?php  
            if(isUserLoggedIn() and $u_a[alliance_id]) {
                echo '<a href="alliance?aid=' . $u_a[alliance_id] . '" class="list-group-item">Alliance Profile: '.$a_a[alliance_name].'</a>';
                echo '<a href="?tab=leave" class="list-group-item">Leave Alliance</a>';
            } elseif(isUserLoggedIn() and !$u_a[alliance_id]) {
                echo '<a href="?tab=create" class="list-group-item">Create an Alliance</a>';
            }
        ?>

    </div>

</div>