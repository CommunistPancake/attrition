<?php
    require_once("models/config.php");
    require_once("db/link_mysql.php");
    require_once("db/pdo.php");
    require_once("common/uiddata.php");

    function bannedcheck($username) {
        global $pdo, $dbprefix, $errors;
        $stmt = $pdo->prepare("SELECT * FROM {$dbprefix}users WHERE username='$username'");
        $stmt->execute();
        $u = $stmt->fetch(PDO::FETCH_ASSOC);

        if($u[banned] == true) {
            $errors[] = 'This account is banned.';
        }
    }
    function getmailcount($user) {
        global $pdo;
        $sql = "SELECT user_id FROM attr_mail WHERE user_id='$user'";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $mailcount = $stmt->rowCount();

        if($stmt->rowCount() > 0) {
            return '<span class="badge">'.$mailcount.'</span>';
        }

    }
    function getuserdata($user) {
        global $pdo;
        $stmt = $pdo->prepare("SELECT * FROM attr_users WHERE user_id='$user'");
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        $u[theme] = $user[theme];
        $u[country_flag] = $user[country_flag];
        return $u;
    }
    function getflagfile($flag) {
        //this should be a database
        if ($flag == "happening") {
            $flagfile = "happening.gif"; //gif
        } else {
            $flagfile = $flag . ".png";
        }
        return '<img style="width: 100%;" src="images/flags/' . $flagfile . '">';
    }
    function getleaderfile($leader) {
        if ($leader == "kebab") {
            $leaderfile = "kebab.gif"; //gif
        } else {
            $leaderfile = $leader . ".png";
        }
        return 'images/leaders/' . $leaderfile;
    }
    function gettheme($user) {
        global $pdo;
        $stmt = $pdo->prepare("SELECT theme FROM attr_users WHERE user_id='$user'");
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        $valid_themes = array("amelia", "cerulean", "cosmo", "cyborg", "flatly", "geo", "journal", "readable", "simplex", "slate", "spacelab", "united",'cirrus', 'white_plum');
        if ($data[theme] == 'default') {
            return '<link rel="stylesheet" href="css/bootstrap.min.css">';
        } else if (in_array($data[theme], $valid_themes)) {
            return '<link rel="stylesheet" href="css/' . $data[theme] . '.min.css">';
        } else {
            return '<link rel="stylesheet" href="css/cyborg.min.css">';
        }
    }
    function alert($alerttype, $string) {
        $valid_alerttypes = array("success", "info", "warning", "danger");
        if (in_array($alerttype, $valid_alerttypes)) {
            return '<div class="alert alert-dismissable alert-' . $alerttype . '">' . $string . '</div>';
        } elseif ($alerttype == 'plain') {
            return '<small>'.$string.'</small>';
        } elseif ($alerttype == 'updates') {
            return '<center><h6>'.$string.'</h6></center>';
        } else {
            return '<div class="alert alert-dismissable alert-danger"> (Invalid Alert Type - Report as Bug!) ' . $string . '</div>';
        }
    }
    function debug($debugtype, $string) {
        $valid_debugtypes = array("success", "info", "warning", "danger");
        if($u_a[debug_mode] == true) {
            if (in_array($debugtype, $valid_debugtypes)) {
                return '<div class="alert alert-dismissable alert-' . $debugtype . '">' . $string . '</div>';
            } else {
                return '<div class="alert alert-dismissable alert-danger"> (Invalid Alert Type - Report as Bug!) ' . $string . '</div>';
            }
        }
    }
    function getleaderprefix($type) {
		if($type == 'authoritarian democracy') {
			return 'President';
		} else if($type == 'dictatorship') {
            return 'Dear Leader';
		} else if($type == 'liberal democracy') {
			return 'President';
		} else if($type == 'military junta') {
			return 'Generalissimo';
		} else if($type == 'one party rule') {
			return 'Premier';
		} else {
            return 'Prefix Unknown (Report this as a bug)';
        }
    }
    function getcountryprefix($type) {
		if($type == 'authoritarian democracy') {
			return 'The Democratic Republic of';
		} else if($type == 'dictatorship') {
			return 'The Great Revolutionary Democratic People\'s Republic of';
		} else if($type == 'liberal democracy') {
			return 'The Republic of';
		} else if($type == 'military junta') {
			return 'The Revolutionary Democratic People\'s Republic of';
		} else if($type == 'one party rule') {
			return 'The Democratic People\'s Republic of';
		} else {
            return 'Prefix Unknown (Report this as a bug)';
        }
    }
    function getqol($user) {
        global $pdo;
        $stmt = $pdo->prepare("SELECT qol FROM attr_users WHERE user_id='$user'");
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user[qol] == 0) {
            return '<span class="text-danger">Police State</span>';
        } else if($user[qol] >= 1 and $user[qol] <= 2) {
            return '<span class="text-warning">Humanitarian Crisis</span>';
        } else if($user[qol] >= 3 and $user[qol] <= 4) {
            return '<span class="text-warning">Below Average</span>';
        } else if($user[qol] == 5) {
            return 'Average';
        } else if($user[qol] >= 6 and $user[qol] <= 7) {
            return 'Above Average';
        } else if($user[qol] == 8) {
            return '<span class="text-success">Excellent<span>';
        } else if($user[qol] >= 9 and $user[qol] <= 10) {
            return '<span class="text-success">Prestige</span>';
        } else {
            return 'Unknown (Report this as a bug)';
        }
    }
    function getrebelthreat($user) {
        global $pdo;
        $stmt = $pdo->prepare("SELECT rebel_threat FROM attr_users WHERE user_id='$user'");
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user[rebel_threat] == 0) {
            return 'None';
        } else if($user[rebel_threat] >= 1 and $user[rebel_threat] <= 2) {
            return 'Peaceful Protests';
        } else if($user[rebel_threat] >= 3 and $user[rebel_threat] <= 4) {
            return 'Violent Protests';
        } else if($user[rebel_threat] >= 5 and $user[rebel_threat] <= 6) {
            return 'Minor Combatants';
        } else if($user[rebel_threat] == 7) {
            return '<span class="text-warning">Guerillas Movements</span>';
        } else if($user[rebel_threat] == 8) {
            return '<span class="text-warning">Inevitable Revolution</span>';
        } else if($user[rebel_threat] >= 9 and $user[rebel_threat] <= 10) {
            return '<span class="text-danger">It\'s Happening</span>';
        } else {
            return 'Unknown (Report this as a bug)';
        }
    }
    function getalignment($user) {
        global $pdo;
        $stmt = $pdo->prepare("SELECT alignment FROM attr_users WHERE user_id='$user'");
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user[alignment] == -2) {
            return '<span class="text-danger">The East</span>';
        } else if($user[alignment] == 2) {
            return '<span class="text-success">The West</span>';
        } else if($user[alignment] > -2 and $user[alignment] < 2) {
            return 'Unaligned';
        } else {
            return 'Unknown (Report this as a bug)';
        }
    }
    function getreputation($user) {
        global $pdo;
        $stmt = $pdo->prepare("SELECT reputation FROM attr_users WHERE user_id='$user'");
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user[reputation] == 0) {
            return '<span class="text-danger">Bringer of Sorrow</span>';
        } else if($user[reputation] >= 1 and $user[reputation] <= 2) {
            return '<span class="text-warning">Harbinger of War</span>';
        } else if($user[reputation] >= 3 and $user[reputation] <= 4) {
            return '<span class="text-warning">Opportunist</span>';
        } else if($user[reputation] == 5) {
            return 'Neutral';
        } else if($user[reputation] >= 6 and $user[reputation] <= 7) {
            return 'Peacekeeper';
        } else if($user[reputation] >= 8 and $user[reputation] <= 9) {
            return '<span class="text-success">Friend of the People</span>';
        } else if($user[reputation] == 10) {
            return '<span class="text-success">Last, Best Hope of Humanity</span>';
        } else {
            return 'Unknown (Report this as a bug)';
        }
    }
    function getmanpower($user) {
        global $pdo;
        $stmt = $pdo->prepare("SELECT manpower FROM attr_users WHERE user_id='$user'");
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user[manpower] == 0) {
            return '<span class="text-danger">Empty</span>';
        } else if($user[manpower] >= 1 and $user[manpower] <= 2) {
            return '<span class="text-warning">Depleted</span>';
        } else if($user[manpower] >= 3 and $user[manpower] <= 4) {
            return '<span class="text-warning">Reserved</span>';
        } else if($user[manpower] == 5) {
            return 'Low';
        } else if($user[manpower] >= 6 and $user[manpower] <= 7) {
            return 'Meager';
        } else if($user[manpower] >= 8 and $user[manpower] <= 9) {
            return '<span class="text-success">Plentiful</span>';
        } else if($user[manpower] == 10) {
            return '<span class="text-success">Untapped</span>';
        } else {
            return 'Unknown (Report this as a bug)';
        }
    }
    function getequipment($user) {
        global $pdo;
        $stmt = $pdo->prepare("SELECT tech FROM attr_users WHERE user_id='$user'");
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user[tech] <= 10) {
            return '<span class="text-danger">World War I Surplus</span>';
        } else if($user[tech] >= 11 and $user[tech] <= 20) {
            return '<span class="text-warning">World War II Surplus</span>';
        } else if($user[tech] >= 21 and $user[tech] <= 40) {
            return '<span>Vietnam Surplus</span>';
        } else if($user[tech] >= 41 and $user[tech] <= 80) {
            return '<span class="text-success">Gulf War Surplus</span>';
        } else if($user[tech] >= 81 and $user[tech] <= 160) {
            return '<span class="text-success">Modern</span>';
        } else {
            return 'Unknown (Report this as a bug)';
        }
    }
    function getequipmentprogress($uid, $user) {
        global $pdo;
        $stmt = $pdo->prepare("SELECT tech FROM attr_users WHERE user_id='$uid'");
        $stmt->execute();
        $uid_a = $stmt->fetch(PDO::FETCH_ASSOC);

        if($uid == $user) {
            if($uid_a[tech] <= 10) {
                return '
                    <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="'.($uid_a[tech]*10).'" aria-valuemin="0" aria-valuemax="10" style="width: '.($uid_a[tech]*10).'%">
                        <span class="sr-only">'.($uid_a[tech]*10).'% Complete (success)</span>
                      </div>
                    </div>
                ';
            } else if($uid_a[tech] >= 11 and $uid_a[tech] <= 20) {
                return '
                    <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="'.$uid_a[tech].'" aria-valuemin="11" aria-valuemax="20" style="width: '.$uid_a[tech].'%">
                        <span class="sr-only">'.$uid_a[tech].'% Complete (success)</span>
                      </div>
                    </div>
                ';
            } else if($uid_a[tech] >= 21 and $uid_a[tech] <= 40) {
                return '
                    <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="'.$uid_a[tech].'" aria-valuemin="21" aria-valuemax="40" style="width: '.$uid_a[tech].'%">
                        <span class="sr-only">'.$uid_a[tech].'% Complete (success)</span>
                      </div>
                    </div>
                ';
            } else if($uid_a[tech] >= 41 and $uid_a[tech] <= 80) {
                return '
                    <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="'.$uid_a[tech].'" aria-valuemin="41" aria-valuemax="80" style="width: '.($uid_a[tech]*10).'%">
                        <span class="sr-only">'.$uid_a[tech].'% Complete (success)</span>
                      </div>
                    </div>
                ';
            } else if($uid_a[tech] >= 81 and $uid_a[tech] <= 160) {
                return '
                    <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="'.$uid_a[tech].'" aria-valuemin="81" aria-valuemax="160" style="width: '.($uid_a[tech]*10).'%">
                        <span class="sr-only">'.$uid_a[tech].'% Complete (success)</span>
                      </div>
                    </div>
                ';
            }
        }
    }
    function gettraining($user) {
        global $pdo;
        $stmt = $pdo->prepare("SELECT training FROM attr_users WHERE user_id='$user'");
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user[training] <= 2) {
            return '<span class="text-danger">Pathetic</span>';
        } else if($user[training] >= 3 and $user[training] <= 4) {
            return '<span class="text-warning">Somali Pirates</span>';
        } else if($user[training] == 5) {
            return 'Standard';
        } else if($user[training] >= 6 and $user[training] <= 7) {
            return 'Capable';
        } else if($user[training] >= 8 and $user[training] <= 9) {
            return '<span class="text-success">Skilled</span>';
        } else if($user[training] == 10) {
            return '<span class="text-success">Elite</span>';
        } else {
            return 'Unknown (Report this as a bug)';
        }
    }
    function getstability($user) {
        global $pdo;
        $stmt = $pdo->prepare("SELECT stability FROM attr_users WHERE user_id='$user'");
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user[stability] <= 2) {
            return '<span class="text-danger">Brink of collapse</span>';
        } else if($user[stability] >= 3 and $user[stability] <= 4) {
            return '<span class="text-warning">Unstable</span>';
        } else if($user[stability] == 5) {
            return 'Stable';
        } else if($user[stability] >= 6 and $user[stability] <= 7) {
            return 'Soild';
        } else if($user[stability] >= 8 and $user[stability] <= 9) {
            return '<span class="text-success">Unshekelable</span>';
        } else if($user[stability] == 10) {
            return '<span class="text-success">Entrenched</span>';
        } else {
            return 'Unknown (Report this as a bug)';
        }
    }
    function getanthem($user) {
        global $pdo;
        $sql = "SELECT anthem_url FROM attr_users WHERE user_id='$user'";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $anthem = $stmt->fetch(PDO::FETCH_ASSOC);
        $anthem_url = $stmt->rowCount();

        if($stmt->rowCount() > 0) {
            return '
                <script src="//www.google.com/jsapi" type="text/javascript"></script>
                <script type="text/javascript">
                    google.load("swfobject", "2.1");
                </script>  
                <script type="text/javascript">
                    function _run() {                
                        // The video to load.
                        var videoID = "'.$anthem[anthem_url].'"
                        // Lets Flash from another domain call JavaScript
                        var params = { allowScriptAccess: "always" };
                        // The element id of the Flash embed
                        var atts = { id: "ytPlayer" };
                        // All of the magic handled by SWFObject (http://code.google.com/p/swfobject/)
                        swfobject.embedSWF("http://www.youtube.com/v/" + videoID + "?version=3&enablejsapi=1&playerapiid=player1&autohide=0&iv_load_policy=3", 
                        "videoDiv", "100%", "10%", "9", null, null, params, atts);      
                    }
                    google.setOnLoadCallback(_run);
                </script>
                <div id="videoDiv">Loading...</div>
            ';
        } else {
            return 'This country has not set an anthem.';
        }

    }
    function getallianceanthem($aid) {
        global $pdo;
        $sql = "SELECT alliance_anthem FROM attr_alliances WHERE alliance_id='$aid'";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $anthem = $stmt->fetch(PDO::FETCH_ASSOC);
        $alliance_anthem = $stmt->rowCount();

        if($stmt->rowCount() > 0) {
            return '
                <script src="//www.google.com/jsapi" type="text/javascript"></script>
                <script type="text/javascript">
                    google.load("swfobject", "2.1");
                </script>  
                <script type="text/javascript">
                    function _run() {                
                        // The video to load.
                        var videoID = "'.$anthem[alliance_anthem].'"
                        // Lets Flash from another domain call JavaScript
                        var params = { allowScriptAccess: "always" };
                        // The element id of the Flash embed
                        var atts = { id: "ytPlayer" };
                        // All of the magic handled by SWFObject (http://code.google.com/p/swfobject/)
                        swfobject.embedSWF("http://www.youtube.com/v/" + videoID + "?version=3&enablejsapi=1&playerapiid=player1&autohide=0&iv_load_policy=3", 
                        "videoDiv", "100%", "10%", "9", null, null, params, atts);      
                    }
                    google.setOnLoadCallback(_run);
                </script>
                <div id="videoDiv">Loading...</div>
            ';
        } else {
            return 'This country has not set an anthem.';
        }

    }
    function getairforce($user) {
        global $pdo;
        $stmt = $pdo->prepare("SELECT air_force FROM attr_users WHERE user_id='$user'");
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user[air_force] <= 10) {
            return '<span class="text-danger">World War I Surplus</span>';
        } else if($user[air_force] >= 11 and $user[air_force] <= 20) {
            return '<span class="text-warning">World War II Surplus</span>';
        } else if($user[air_force] >= 21 and $user[air_force] <= 40) {
            return '<span>Vietnam Surplus</span>';
        } else if($user[air_force] >= 41 and $user[air_force] <= 80) {
            return '<span class="text-success">Gulf War Surplus</span>';
        } else if($user[air_force] >= 81 and $user[air_force] <= 160) {
            return '<span class="text-success">Modern</span>';
        } else {
            return 'Unknown (Report this as a bug)';
        }
    }
?>