<?php
  	require_once("common/ungit/config.php");
  	
  	try {
	  	// Database Connection
	  	$pdo = new PDO("mysql:host=$dbhost;dbname=$databasename", $databaseusername, $databasepassword);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch (PDOException $e) {
	    print "Error!: " . $e->getMessage() . "<br/>";
	    die();
	}
?>