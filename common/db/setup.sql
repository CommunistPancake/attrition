SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `attr_alliances` (
  `alliance_id` int(11) NOT NULL AUTO_INCREMENT,
  `founder_id` int(11) NOT NULL,
  `alliance_name` varchar(150) NOT NULL,
  `description` varchar(150) DEFAULT NULL,
  `est_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `alliance_flag` varchar(150) NOT NULL,
  PRIMARY KEY (`alliance_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attr_awards`
--

CREATE TABLE IF NOT EXISTS `attr_awards` (
  `award_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `award_name` varchar(150) NOT NULL,
  `award_reason` varchar(150) DEFAULT NULL,
  `award_int` int(11) DEFAULT NULL,
  `awarded_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`award_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attr_events`
--

CREATE TABLE IF NOT EXISTS `attr_events` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` varchar(150) NOT NULL,
  `event_action` varchar(150) DEFAULT NULL,
  `attacker_id` int(11) DEFAULT NULL,
  `defender_id` int(11) DEFAULT NULL,
  `event_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attr_groups`
--

CREATE TABLE IF NOT EXISTS `attr_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(225) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attr_mail`
--

CREATE TABLE IF NOT EXISTS `attr_mail` (
  `mail_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `mail_type` varchar(150) NOT NULL,
  `mail_subtype` varchar(150) DEFAULT NULL,
  `string` varchar(512) DEFAULT NULL,
  `mail_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mail_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attr_sessions`
--

CREATE TABLE IF NOT EXISTS `attr_sessions` (
  `session_start` int(11) NOT NULL,
  `session_data` text NOT NULL,
  `session_id` varchar(255) NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attr_users`
--

CREATE TABLE IF NOT EXISTS `attr_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `username_clean` varchar(150) NOT NULL,
  `password` varchar(225) NOT NULL,
  `email` varchar(150) NOT NULL,
  `activationtoken` varchar(225) NOT NULL,
  `last_activation_request` int(11) NOT NULL,
  `LostpasswordRequest` int(1) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL,
  `group_id` int(11) NOT NULL,
  `sign_up_date` int(11) NOT NULL,
  `last_sign_in` int(11) NOT NULL,
  `country_name` varchar(150) NOT NULL,
  `country_flag` varchar(150) NOT NULL,
  `gov_type` varchar(150) NOT NULL DEFAULT 'Liberal Democracy',
  `eco_type` varchar(150) NOT NULL DEFAULT 'Free Market',
  `leader` varchar(150) NOT NULL DEFAULT 'default',
  `region` varchar(150) NOT NULL,
  `territory` int(11) NOT NULL DEFAULT '20000',
  `description` varchar(256) DEFAULT NULL,
  `stability` int(2) NOT NULL DEFAULT '5',
  `funds` int(11) NOT NULL DEFAULT '500000',
  `gdp` int(11) NOT NULL DEFAULT '50000000',
  `growth` int(11) NOT NULL DEFAULT '1000000',
  `foreign_investment` int(11) DEFAULT NULL,
  `population` bigint(20) NOT NULL DEFAULT '8000000',
  `qol` int(2) NOT NULL DEFAULT '5',
  `approval` int(2) NOT NULL DEFAULT '5',
  `rebel_threat` int(2) NOT NULL DEFAULT '0',
  `alliance_id` int(11) DEFAULT NULL,
  `alignment` int(2) NOT NULL DEFAULT '0',
  `reputation` int(2) NOT NULL DEFAULT '5',
  `troops` int(11) NOT NULL DEFAULT '10000',
  `manpower` int(2) NOT NULL DEFAULT '10',
  `training` float NOT NULL DEFAULT '1',
  `tech` float NOT NULL DEFAULT '0',
  `safe_mode` tinyint(1) NOT NULL DEFAULT '1',
  `debug_mode` tinyint(1) NOT NULL DEFAULT '0',
  `donor` tinyint(1) NOT NULL DEFAULT '0',
  `theme` varchar(150) NOT NULL DEFAULT 'cerulean',
  `overwrite_theme` tinyint(1) NOT NULL DEFAULT '1',
  `has_prospected` tinyint(1) NOT NULL DEFAULT '0',
  `oil_production` int(11) NOT NULL DEFAULT '0',
  `oil_reserves` int(11) NOT NULL DEFAULT '5',
  `has_attacked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;