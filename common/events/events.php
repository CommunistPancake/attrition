<?
	set_include_path('/home/playagne/public_html/at.playag.net/');
	require_once("common/db/link_mysql.php");
	require_once("common/db/pdo.php");

	$result = mysql_query("SELECT * FROM {$dbprefix}users WHERE active='1'", $link);

	$rows = mysql_num_rows($result);
  	$user = mysql_fetch_assoc($result);
	
	do {
		// Natural Disasters
		// population, qol, oil_res, oil_prod, stability, rebel
		$population = $user[population];
		$qol = $user[qol];
		$oil_reserves = $user[oil_reserves];
		$oil_production = $user[oil_production];
		$rebel_threat = $user[rebel_threat];

		if($user[region] == 'africa') {
			$possibility = mt_rand(1, 30);
			$disaster_title = 'famine';

			if($user[reputation] < 7) {		
				$disaster_outcome = 'Your people are starving. Stability is shaken and rebels increase.';
				if($user[stability] >= 1 ) {
					$stability = $user[stability] - 1;
				}
				if($user[rebel_threat] < 10 ) {
					$rebel_threat = $user[rebel_threat] + 1;
				}
			} else {
				$disaster_outcome = 'Your people are starving. Your reputation is high and you are not negatively effected.';
			}
		}
		if($user[region] == 'asia') {
			$possibility = mt_rand(1, 30);
			$disaster_title = 'typhoon';

			if($user[stability] >= 1 ) {
				$disaster_outcome = 'Your rice villages are hit by a powerful typhoon. Stability is slightly reduced.';
				$stability = $user[stability] - 1;
			} else {
				$disaster_outcome = 'Your rice villages are hit by a weak typhoon. You are lucky enough to not lose stability.';
			}
		}
		if($user[region] == 'europe') {
			$possibility = mt_rand(1, 30);
			$disaster_title = 'immigration';

			if($user[qol] >= 8) {
				$disaster_outcome = 'Thousands of Muslims seek refuge and welfare in your country. Quality of Life slightly decreases.';
				$population = $user[population] + 500000;
				$qol = $user[qol] - 1;
			}
		}
		if($user[region] == 'middle east') {
			$possibility = mt_rand(1, 30);
			$disaster_title = 'U.S. Invasion';

			if($user[oil_production] >= 1) {
				$disaster_outcome = 'The U.S. has decided to liberate you. Oil production has been reduced by 1.';
				$oil_production = $user[oil_production] - 1;
			} elseif($user[oil_reserves] >= 5) {
				$disaster_outcome = 'The U.S. has decided to liberate you. No oil production is found and 5 Mbbl of your reserves are taken.';
				$oil_reserves = $user[oil_reserves] - 5;
			} else {
				$disaster_outcome = 'The U.S. has decided to liberate you. No oil production or reserves are found and the U.S. withdraws without financially destroying your asshole.';
			}
		}
		if($possibility == 1) {
			echo ucwords($disaster_title).'<br>'.$disaster_outcome.'<br>';
			// echo $population.' '.$qol.' '.$oil_reserves.' '.$oil_production.' '.$stability.' '.$rebel_threat.' ';

			// population, qol, oil_res, oil_prod, stability, rebel
	    	$sql = ("UPDATE {$dbprefix}users SET population = :population, qol = :qol, oil_reserves = :oil_reserves, oil_production = :oil_production, stability = :stability, rebel_threat = :rebel_threat WHERE user_id='$user[user_id]'");
	    	$stmt = $pdo->prepare($sql);

		   	$stmt->bindParam(':population', $population, PDO::PARAM_INT);
		    $stmt->bindParam(':qol', $qol, PDO::PARAM_INT);
		    $stmt->bindParam(':oil_reserves', $oil_reserves, PDO::PARAM_INT);
		    $stmt->bindParam(':oil_production', $oil_production, PDO::PARAM_INT);
		    $stmt->bindParam(':stability', $stability, PDO::PARAM_INT);
		    $stmt->bindParam(':rebel_threat', $rebel_threat, PDO::PARAM_INT);
		    $stmt->execute();

	      	$sql = ("INSERT INTO {$dbprefix}mail (user_id, mail_type, mail_subtype, title, string) VALUES (:user_id, :mail_type, :mail_subtype, :title, :string)");
	      	$stmt = $pdo->prepare($sql);

	      	$mail_type = 'event';
	      	$mail_subtype = 'disaster';
	    	$stmt->bindParam(':user_id', $user[user_id], PDO::PARAM_INT);
	    	$stmt->bindParam(':mail_type', $mail_type, PDO::PARAM_STR); 
	    	$stmt->bindParam(':mail_subtype', $mail_subtype, PDO::PARAM_STR); 
	    	$stmt->bindParam(':title', ucwords($disaster_title), PDO::PARAM_STR);
	    	$stmt->bindParam(':string', ucfirst($disaster_outcome), PDO::PARAM_STR);
	    	$stmt->execute();

            $sql = "INSERT INTO {$dbprefix}events (event_type,event_action,attacker_id) VALUES (:event_type,:event_action,:attacker_id)";
            $stmt = $pdo->prepare($sql);

            $event_type = 'disaster';
            $event_action = 'Has received the '.ucwords($disaster_title).' random event.';
            $stmt->bindParam(':event_type', $event_type, PDO::PARAM_STR);
            $stmt->bindParam(':event_action', $event_action, PDO::PARAM_STR);
            $stmt->bindParam(':attacker_id', $user[user_id], PDO::PARAM_INT);
            $stmt->execute();
		} else {
			// Good event
		}

	} while ($user = mysql_fetch_assoc($result));
?>