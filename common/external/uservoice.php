<!-- UserVoice JavaScript SDK (only needed once on a page) -->
<script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/MF5jegLOgF25n6tBNWzFAA.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script>

<!-- A tab to launch the Classic Widget -->
<script>
UserVoice = window.UserVoice || [];
UserVoice.push(['showTab', 'classic_widget', {
  mode: 'full',
  primary_color: '#007cbf',
  link_color: '#007cbf',
  default_mode: 'feedback',
  forum_id: 230512,
  tab_label: 'Feedback & Support',
  tab_color: '#0063cc',
  tab_position: 'middle-right',
  tab_inverted: false
}]);
</script>