<div class="row">
    <? require_once("common/policies/sidebar.php"); ?>
    <div class="col-md-8">

    <? if($u_a[intel_agency] == null) { ?>
        <!-- Create an Intelligence Agency -->
        <div class="panel panel-default">
            <div class="panel-body">
            <h4>Create an Intelligence Agency</h4>
            <hr>
                Create an agency to spy on your people and wreak havoc in other countries.
            </div>
            <div class="panel-footer"><small><p class="text-muted">
                
            <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=domestic" method="post">
                <p>
                    <div class="input-group input-group">
                      <span class="input-group-addon">Select This Policy ($<? echo $cost = number_format($growth_cost = (0.005*$u_a[gdp]) + (0.005*floor($u_a[gdp]*.000000001))); ?>)</span>
                      <input type="text" class="form-control" placeholder="Enter a name for your agency." name="agency_name" <? if($u_a[funds] >= $cost) { echo 'id="inputSuccess"'; } else { echo 'id="inputError"'; } ?>>
                    </div>
                    <!-- <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy (250,000)" name="create_intel_agency"> -->
                </p>
            </form>
            </p></small></div>
        </div>
    <? } ?>

        <!-- Build a University -->
        <div class="panel panel-default">
            <div class="panel-body">
            <h4>Build a University</h4>
            <hr>
                Open the gates of a new institution for higher learning to your people.
            </div>
            <div class="panel-footer"><small><p class="text-muted">
                Costs $500K growth and $250K funds for endowment. Raises quality of life moderately.
            <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=domestic" method="post">
                <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy (250,000)" name="build_uni">
                </p>
            </form>
            </p></small></div>
        </div>

        <!-- Convert unemployed population -->
        <div class="panel panel-default">
            <div class="panel-body">
            <h4>Convert The Undesirables to Crude Oil</h4>
            <hr>
                These leeches are too dependent on the government. Dispose of them.
            </div>
            <div class="panel-footer"><small><p class="text-muted">
                Produces 5 Mbble of oil and reduces the population by 100,000. Reduces reputation, stability and quality of life. Chance of increasing rebels.
            <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=domestic" method="post">
                <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy (Free)" name="convert_population">
                </p>
            </form>
            </p></small></div>
        </div>

        <!-- Arrest Opposition and Would-Be Revolutionaries -->
        <div class="panel panel-default">
            <div class="panel-body">
            <h4>Arrest Opposition and Would-Be Revolutionaries</h4>
            <hr>
                These ignorant fools do nothing but criticize. Shut them up.
            </div>
            <div class="panel-footer"><small><p class="text-muted">
                Your government will become more authoritarian. Chance of decreasing rebels. Slightly increases stability.
            <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=domestic" method="post">
                <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy ($50,000)" name="arrest_op">
                </p>
            </form>
            </p></small></div>
        </div>

        <!-- Convert foreign villages -->
        <div class="panel panel-default">
            <div class="panel-body">
            <h4>Expand Territory</h4>
            <hr>
                Convert foreign villages
            </div>
            <div class="panel-footer"><small><p class="text-muted">
                50% Expansion/+ 100 km of territory, 35% Failure, 10% Retaliation/- 100 km, 5% Great Success/+ 500 km.
            <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=domestic" method="post">
                <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy ($100,000)" name="expand_territory">
                </p>
            </form>
            </p></small></div>
        </div>

        <!-- Green Card Rush -->
        <div class="panel panel-default">
            <div class="panel-body">
            <h4>Green Card Rush</h4>
            <hr>
                Hand out worker visas and residencies like candy. Candy with a price (you'll collect later).
            </div>
            <div class="panel-footer"><small><p class="text-muted">
                75% chance of increasing Manpower one step. 20% of two steps. 5% of three steps. 35% of increasing Reputation slightly.
            <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=domestic" method="post">
                <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy ($<? echo number_format(300000 + $u_a[troops]*10); ?>)" name="green_card">
                </p>
            </form>
            </p></small></div>
        </div>

        <!-- Natural Resource Rampage -->
        <div class="panel panel-default">
            <div class="panel-body">
            <h4>Natural Resource Rampage</h4>
            <hr>
                Scour and rape your nation's natural wealth and beauty to find sources of unsustainable energy.
            </div>
            <div class="panel-footer"><small><p class="text-muted">
                Random chance of gaining Mbbl's of Oil (0-5 Mbbl) one time.
            <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=domestic" method="post">
                <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy ($100,000)" name="resource_rampage">
                </p>
            </form>
            </p></small></div>
        </div>

    </div>
</div>