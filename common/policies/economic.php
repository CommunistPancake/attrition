<div class="row">
  <? require_once("common/policies/sidebar.php"); ?>

    <div class="col-md-8">
        <?
            require_once("common/alerts.php");

            $result = mysql_query("SELECT * FROM {$dbprefix}economy WHERE commodity='oil'", $link);
            $result = mysql_fetch_assoc($result);
        ?>
        <center><h4>Current price per Mbbl: $<? echo number_format($result[var1]); ?>.</h4></center>
        <hr>

          <div class="panel panel-default">
            <div class="panel-body">
            <h4>Export Oil</h4>
            <hr>
            Sell some of your precious oil reserves. There is a $100 processing fee.
            </div>
            <div class="panel-footer"><small><p class="text-muted">
            Adds funds based on the current market price.
                <form action="<? echo $_SERVER['PHP_SELF']?>?type=economic" method="post">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="modal fade" id="export_oil" tabindex="-1" role="dialog" aria-labelledby="export_oil" aria-hidden="true">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" id="export_oil">Confirm</h4>
                                  </div>
                                  <div class="modal-body">
                                      <input type="submit" class="btn btn-danger btn-block" value="Really use this policy?" name="export_oil"/>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <p>
                        <div class="input-group input-group">
                          <span class="input-group-addon">Select This Policy (Free)</span>
                          <input type="text" class="form-control" placeholder="Enter a number of millions of barrels to sell." name="x">
                        </div>
                      </p>
                    </div>
                  </div>
                </form> 
             </p></small></div>
          </div>

          <div class="panel panel-default">
            <div class="panel-body">
            <h4>Import Oil</h4>
            <hr>
            Purchase oil from other countries. There is a $100 processing fee.
            </div>
            <div class="panel-footer"><small><p class="text-muted">
            Purchase oil based on the current market price.
                <form action="<? echo $_SERVER['PHP_SELF']?>?type=economic" method="post">
                  <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="
                    <?
                        $result = mysql_query("SELECT * FROM {$dbprefix}economy WHERE commodity='oil'", $link);
                        $market = mysql_fetch_assoc($result);
                        echo 'Select This Policy ($'.number_format($market[var1]).')';
                    ?>
                    " name="import_oil">
                  </p>
                </form>
             </p></small></div>
          </div>

          <div class="panel panel-default">
            <div class="panel-body">
            <h4>Streamline Production Quotas</h4>
            <hr>
            Influence the GDP of your country by working your people to death.
            </div>
            <div class="panel-footer"><small><p class="text-muted">
            Develop growth with a 70% chance of success and  30% chance of no growth. Not available to Free Market economies.
                <form action="<? echo $_SERVER['PHP_SELF']?>?type=economic" method="post">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="modal fade" id="glf" tabindex="-1" role="dialog" aria-labelledby="glf" aria-hidden="true">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" id="glf">Confirm</h4>
                                  </div>
                                  <div class="modal-body">
                                      <input type="submit" class="btn btn-danger btn-block" value="Really use this policy?" name="glf"/>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <p>
                        <div class="input-group input-group">
                          <span class="input-group-addon">Select This Policy ($<? echo number_format($growth_cost = (0.001*$u_a[gdp]) + (0.001*floor($u_a[gdp]*.000000001))); ?>)</span>
                          <input type="text" class="form-control" placeholder="Enter a number of times to run this policy." name="x">
                        </div>
                      </p>
                    </div>
                  </div>
                </form> 
             </p></small></div>
          </div>

          <div class="panel panel-default">
            <div class="panel-body">
            <h4>Tax Incentives!</h4>
            <hr>
            Invite rich Yankees to exploit your cheap labor and resources.

            </div>

            <div class="panel-footer"><small><p class="text-muted">

            Slight chance of increasing economic growth... but the rest could always be nationalized later. Not available to Eastern aligned countries.

                <form action="<? echo $_SERVER['PHP_SELF']?>?type=economic" method="post">
                  <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy ($<? echo $cost = number_format((0.00075*$u_a[gdp]) + (.00075*floor($u_a[gdp]*.000000001))); ?>)" name="foreign_investment">
                  </p>
                </form>
             
             </p></small></div>

          </div>

          <div class="panel panel-default">
            <div class="panel-body">
            <h4>Nationalize Foreign Investment</h4>
            <hr>
            Take from the rich Yankees what is rightfully yours.

            </div>

            <div class="panel-footer"><small><p class="text-muted">

            Seizure of all foreign investment is added directly to your funds, but alienates the US and moves your nation closer towards the left. Significantly reduces stability.

                <form action="<? echo $_SERVER['PHP_SELF']?>?type=economic" method="post">
                  <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy (Free)" name="nationalize_investment">
                  </p>
                </form>
             
             </p></small></div>

          </div>

          <div class="panel panel-default">
            <div class="panel-body">
            <h4>Privatize</h4>
            <hr>
            Sell off state assets to the highest bidder.

            </div>

            <div class="panel-footer"><small><p class="text-muted">

            A 300,000 to your funds, move you closer to the free market, and reduce your GDP by $15 million. Significantly reduces stability.

                <form action="<? echo $_SERVER['PHP_SELF']?>?type=economic" method="post">
                  <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy (Free)" name="privatize">
                  </p>
                </form>
             
             </p></small></div>

          </div>

          <div class="panel panel-default">
            <div class="panel-body">
            <h4>Construct Oil Well</h4>
            <hr>
            Extract black gold from your once beautiful country.

            </div>

            <div class="panel-footer"><small><p class="text-muted">

            Constructs one oil well, producing 1 Mbbl (one thousand barrels) per month.

                <form action="<? echo $_SERVER['PHP_SELF']?>?type=economic" method="post">
                  <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy ($500,000)" name="prospect">
                  </p>
                </form>
             
             </p></small></div>

          </div>
  </div>
</div>