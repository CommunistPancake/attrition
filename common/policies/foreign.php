<div class="row">
  <? require_once("common/policies/sidebar.php"); ?>
  <div class="col-md-8">

    <div class="panel panel-default">
      <div class="panel-body">
        <h4>Humanitarian Aid</h4>
        <hr>
        Beg a foreign superpower to open an aid camp in your country.
      </div>
      <div class="panel-footer"><small><p class="text-muted">
        Improves quality of life slightly. Lowers reputation slightly.
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=foreign" method="post">
          <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy (Free)" name="humanitarian_aid">
        </form>
      </p></small></div>
    </div>

    <div class="panel panel-default">
      <div class="panel-body">
        <h4>Accept Foreign Refugees</h4>
        <hr>
        Let some oppressed and starved refugees find another chance in your nation.
      </div>
      <div class="panel-footer"><small><p class="text-muted">
        Not available in authoritarian nations. Adds population at the expense of a slight decrease in quality of life. Slight chance of nationalists increasing rebels.
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=foreign" method="post">
          <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy (Free)" name="accept_refugees">
        </form>
      </p></small></div>
    </div>

    <div class="panel panel-default">
      <div class="panel-body">
        <h4>Praise the Soviet Union</h4>
        <hr>
        Give the General Secretary a bear hug in front of the cameras.
      </div>
      <div class="panel-footer"><small><p class="text-muted">
        Increases your relations with the godless communists, hurts them with the freedom-loving West. Stability decreases somewhat as the bourgeoisie bitches.
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=foreign" method="post">
          <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy ($50,000)" name="praise_east">
        </form>
      </p></small></div>
    </div>
    
    <div class="panel panel-default">
      <div class="panel-body">
        <h4>Praise the United States</h4>
        <hr>
        Shake the Presidents hand on the tarmac.
      </div>
      <div class="panel-footer"><small><p class="text-muted">
        Increases your relations with the bloodthirsty capitalists, hurts them with the equality-loving Soviets. Stability decreases somewhat as all the pinkos complain.
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=foreign" method="post">
          <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy ($50,000)" name="praise_west">
        </form>
      </p></small></div>
    </div>
  </div>

</div>