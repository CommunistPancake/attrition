<div class="col-md-4">
    <div class="list-group">
        <a href="policies?type=economic" class="list-group-item <? if($type == 'economic') echo 'active'; ?>">Economic</a>
        <a href="policies?type=domestic" class="list-group-item <? if($type == 'domestic') echo 'active'; ?>">Domestic</a>
        <a href="policies?type=foreign" class="list-group-item <? if($type == 'foreign') echo 'active'; ?>">Foreign</a>
        <a href="policies?type=military" class="list-group-item <? if($type == 'military') echo 'active'; ?>">Military</a>
    </div>
    <hr>
<!--     <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo getcountryprefix($u_a[gov_type]) . ' <u><a href="user.php?uid='.$u_a[user_id].'">'.stripcslashes(ucwords($u_a[country_name])).'</a></u>'; ?></h3>
        </div>
        <div class="panel-body">
            <center>
                <?
                    if($u_a[custom_flag]) {
                        echo '<img style="width: 100%;" src="'.$u_a[custom_flag].'">';
                    } else {
                        echo getflagfile($u_a[country_flag]);
                    }
                ?>
            </center> -->

            <table class="table table-hover table-striped">
                <tbody>
                    <tr>
                        <td><h6 class="text-success">Funds</h6></td>
                        <td><h6 class="text-muted">$<? echo number_format($u_a[funds]); ?></h6></td>
                    </tr>
                    <tr>
                        <td><h6 class="text-success">GDP</h6></td>
                        <td><h6 class="text-muted">$<?php echo $english_format_number = number_format($u_a[gdp]); ?></h6></td>
                    </tr>
                    <tr>
                        <td><h6 class="text-success">Growth</td>
                        <td><h6 class="text-muted">$<?php echo $english_format_number = number_format($u_a[growth]); ?></h6></td>
                    </tr>
                    <tr>
                        <td><h6 class="text-success">Foreign Investment</td>
                        <td><h6 class="text-muted">
                        <?
                          if($u_a[foreign_investment] > 0) {
                            echo '$' . $english_format_number = number_format($u_a[foreign_investment]);
                          } elseif($u_a[foreign_investment] <= 0) {
                            echo 'None';
                          }
                        ?>
                        </h6></td>
                    </tr>
                    <tr>
                        <td><h6 class="text-success">Oil Reserves</h6></td>
                        <td><h6 class="text-muted"><?php echo $english_format_number = number_format($u_a[oil_reserves]).' Mbbl'; ?></h6></td>
                    </tr>
                    <tr>
                        <td><h6 class="text-info">Territory</td>
                        <td><h6 class="text-muted"><?php echo $english_format_number = number_format($u_a[territory]) . ' km<sup>2</sup>'; ?></h6></td>
                    </tr>
                    <tr>
                        <td><h6 class="text-info">Population</td>
                        <td><h6 class="text-muted"><?php echo $english_format_number = number_format($u_a[population]); ?></h6></td>
                    </tr>
                    <tr>
                        <td><h6 class="text-info">Stability</td>
                        <td><h6 class="text-muted"><?php echo getstability($u_a[user_id]) ?></h6></td>
                    </tr>
                    <tr>
                        <td><h6 class="text-warning">Alignment</td>
                        <td><h6 class="text-muted"><?php echo getalignment($u_a[user_id]) ?></h6></td>
                    </tr>
                    <tr>
                        <td><h6 class="text-warning">Region</td>
                        <td><h6 class="text-muted"><?php echo stripcslashes(ucwords($u_a[region])); ?></h6></td>
                    </tr>
                    <tr>
                        <td><h6 class="text-warning">Reputation</td>
                        <td><h6 class="text-muted"><?php echo getreputation($u_a[user_id]) ?></h6></td>
                    </tr>
                    <tr>
                        <td><h6 class="text-danger">Troops</td>
                        <td><h6 class="text-muted"><?php echo $english_format_number = number_format($u_a[troops]); ?></h6></td>
                    </tr>
                    <tr>
                        <td><h6 class="text-danger">Manpower</td>
                        <td><h6 class="text-muted"><?php echo getmanpower($u_a[user_id]) ?></h6></td>
                    </tr>
                    <tr>
                        <td><h6 class="text-danger">Equipment</td>
                        <td><h6 class="text-muted"><?php echo getequipment($u_a[user_id]) ?></h6></td>
                    </tr>
                    <tr>
                        <td><h6 class="text-danger">Training</td>
                        <td><h6 class="text-muted"><?php echo gettraining($u_a[user_id]) ?></h6></td>
                    </tr>
                </tbody>
            </table>

<!--         </div>
    </div> -->
</div>