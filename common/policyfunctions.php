<?
    // Economic
    if(isset($_POST['export_oil'])) {
        $x = trim($_POST["x"]);
        $x = filter_var($x, FILTER_SANITIZE_NUMBER_INT);
        if($x == null) {
          $x = 1;
        }
        if($x < 1) {
          $errors[] = 'You must enter a positive number.';
        }
        if($u_a[oil_reserves] < $x) {
          $errors[] = 'You do not have enough oil to export <b>'.$x.'</b> Mbbls.';
        }
        $result = mysql_query("SELECT * FROM {$dbprefix}economy WHERE commodity='oil'", $link);
        $market = mysql_fetch_assoc($result);
        $var1_origin = $market[var1];
        $x_origin = $x;
        if(!$errors) {
            do {
                require("userdata.php");
                $x = $x - 1;
                $result = mysql_query("SELECT * FROM {$dbprefix}economy WHERE commodity='oil'", $link);
                $market = mysql_fetch_assoc($result);
                $total_price = $total_price + $market[var1];

                $sql = ("UPDATE {$dbprefix}users SET funds = :funds, oil_reserves = :oil_reserves WHERE user_id='$u_a[user_id]'");
                $stmt = $pdo->prepare($sql);

                $funds = $u_a[funds] + $market[var1] - 100;
                $oil_reserves = $u_a[oil_reserves] - 1;
                $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
                $stmt->bindParam(':oil_reserves', $oil_reserves, PDO::PARAM_INT);
                $stmt->execute();

                $sql = ("UPDATE {$dbprefix}economy SET var1 = :var1, var2 = :var2 WHERE commodity='oil'");
                $stmt = $pdo->prepare($sql);

                if($market[var1] > 200) {
                  $var1 = $market[var1] - 200;
                }
                $var2 = $market[var2] + 1;
                $stmt->bindParam(':var1', $var1, PDO::PARAM_INT);
                $stmt->bindParam(':var2', $var2, PDO::PARAM_INT);
                $stmt->execute();

            } while($x > 0);
            $outcome_good[] = 'You have successfully exported <b>'.$x_origin.'</b> Mbbl of oil in exchange for <b>$'.number_format($total_price).'</b> in funds.';
            $outcome_bad[] = 'The price of oil has dropped from <b>$'.number_format($var1_origin).'</b> to <b>$'.number_format($var1).'</b> per Mbbl.';


        }
    }
    if(isset($_POST['import_oil'])) {
        $result = mysql_query("SELECT * FROM {$dbprefix}economy WHERE commodity='oil'", $link);
        $market = mysql_fetch_assoc($result);
        $cost = $market[var1];
        if($market[var2] <= 0) {
            $errors[] = 'There is currently no oil available for import.';
        }

        if($u_a[funds] < $cost) {
          $errors[] = 'You do not have enough funds to import oil.';
        }
        if(!$errors) {
            $sql = ("UPDATE {$dbprefix}users SET funds = :funds, oil_reserves = :oil_reserves WHERE user_id='$u_a[user_id]'");
            $stmt = $pdo->prepare($sql);

            $funds = $u_a[funds] - $market[var1] - 100;
            $oil_reserves = $u_a[oil_reserves] + 1;
            $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
            $stmt->bindParam(':oil_reserves', $oil_reserves, PDO::PARAM_INT);
            $stmt->execute();

            $sql = ("UPDATE {$dbprefix}economy SET var1 = :var1, var2 = :var2 WHERE commodity='oil'");
            $stmt = $pdo->prepare($sql);

            $var1 = $market[var1] + 200;
            $var2 = $market[var2] - 1;
            $stmt->bindParam(':var1', $var1, PDO::PARAM_INT);
            $stmt->bindParam(':var2', $var2, PDO::PARAM_INT);
            $stmt->execute();

            $outcome_good[] = 'You have successfully imported <b>1</b> Mbbl of oil in exchange for <b>$'.number_format($cost).'</b> in funds.';
            $outcome_good[] = 'The price of oil has increased from <b>$'.number_format($cost).'</b> to <b>$'.number_format($var1).'</b> per Mbbl.';   
        }
    }

  if(isset($_POST['glf'])) {
    $x = trim($_POST["x"]);
    $x = filter_var($x, FILTER_SANITIZE_NUMBER_INT);
    $cost = (0.001*$u_a[gdp]) + (0.001*floor($u_a[gdp]*.000000001));
    if($cost <= 100000) {
      $cost = 100000;
    }
    if($x == null) {
      $x = 1;
    }
    if($x < 1) {
      $errors[] = 'You must enter a positive number.';
    }
    if($u_a[eco_type] == 'free market') {
      $errors[] = 'Free market economies can not complete this policy.';
    }
    if($u_a[funds] < $cost*$x) {
      $errors[] = 'You need: $'.number_format($cost*$x).' funds to complete this policy.';
    }
    if(!$errors) {
      do {
        // echo alert(info, $x);
        require("userdata.php");
        $x = $x - 1;

        $outcome = mt_rand(1, 10);
        if($outcome <= 7) {
          $sql = ("UPDATE {$dbprefix}users SET growth = :growth, funds = :funds WHERE user_id='$u_a[user_id]'");
          $stmt = $pdo->prepare($sql);

          $amount = ( $u_a[growth] + 1000000 );
          $funds = $u_a[funds] - $cost;
          $stmt->bindParam(':growth', $amount, PDO::PARAM_INT);
          $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
          $stmt->execute();
          $has_growth = $has_growth + 1;
        } elseif($outcome >= 8) {
          $sql = ("UPDATE {$dbprefix}users SET funds = :funds WHERE user_id='$u_a[user_id]'");
          $stmt = $pdo->prepare($sql);

          $funds = $u_a[funds] - $cost;
          $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
          $stmt->execute();
          $no_growth = $no_growth + 1;
        }
      } while($x > 0);
      if($no_growth) {
        $outcome_neutral[] = 'Your country has not experienced growth x'.$no_growth.'.';
      }
      if($has_growth) {
        $outcome_good[] = 'Your country has experienced growth x'.$has_growth.'.';
      }
    }
  }
  if(isset($_POST['foreign_investment'])) {
    // Config
    $cost = (0.00075*$u_a[gdp]) + (.00075*floor($u_a[gdp]*.000000001));
    if($cost <= 75000) {
      $cost = 75000;
    }
    if($u_a[funds] < $cost) {
      $errors[] = 'You do not have enough funds to complete this policy.';
    }
    if($u_a[alignment] < -1) {
      $errors[] = 'Eastern aligned countries cannot complete this policy.';
    }


    if(!$errors) {
      $outcome = mt_rand(1, 9);

      if($outcome <= 3) {
        $sql = ("UPDATE {$dbprefix}users SET growth = :growth, funds = :funds, foreign_investment = :foreign_investment WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        $growth = ( $u_a[growth] + 500000 );
        $funds = $u_a[funds] - $cost;
        $investment = round(rand($cost / 1.2, $cost / 1.8));
        $foreign_investment = $u_a[foreign_investment] + $investment;

        // Bind
        $stmt->bindParam(':growth', $growth, PDO::PARAM_INT);
        $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
        $stmt->bindParam(':foreign_investment', $foreign_investment, PDO::PARAM_INT);
        $stmt->execute();

        $outcome_good[] = 'Foreign investment trickles in. Your economy and foreign investment grow. ($' . $english_format_number = number_format($investment) . ')';

      } elseif($outcome >= 4 and $outcome <= 6) {
        $sql = ("UPDATE {$dbprefix}users SET funds = :funds, foreign_investment = :foreign_investment WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        $funds = $u_a[funds] - $cost;
        $investment = round(rand($cost / 2.2, $cost / 2.8));
        $foreign_investment = $u_a[foreign_investment] + $investment;

        // Bind
        $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
        $stmt->bindParam(':foreign_investment', $foreign_investment, PDO::PARAM_INT);
        $stmt->execute();

        $outcome_neutral[] = 'Few deals are made, resulting in no economic growth and little investment. ($' . $english_format_number = number_format($investment) . ')';
      } elseif($outcome >= 7) {
        $sql = ("UPDATE {$dbprefix}users SET funds = :funds WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        $funds = $u_a[funds] - $cost;

        // Bind
        $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
        $stmt->execute();

        $outcome_bad[] = 'No one accepts the offer.';
      }
    }
  }

  if(isset($_POST['nationalize_investment'])) {

    $cost = 0;

    if($u_a[funds] >= $cost and $u_a[foreign_investment] > 0) {

        $sql = ("UPDATE {$dbprefix}users SET funds = :funds, foreign_investment = :foreign_investment, alignment = :alignment WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        $investment = $u_a[foreign_investment];
        $funds = $u_a[funds] + $investment;
        $foreign_investment = 0;

        if($u_a[alignment] >= -1 ) {

          $alignment = $u_a[alignment] - 1;

        } else {

          $alignment = $u_a[alignment];

        }
        
        // Bind
        $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
        $stmt->bindParam(':foreign_investment', $foreign_investment, PDO::PARAM_INT);
        $stmt->bindParam(':alignment', $alignment, PDO::PARAM_INT);
        $stmt->execute();

        $outcome_good[] = 'Seizure of all foreign investment is added directly to your funds. ($' . $english_format_number = number_format($investment) . ')';

    } elseif($u_a[foreign_investment] <= 0) {

      $outcome_bad[] = 'You have no investments to seize.';

    } else {

      $outcome_bad[] = 'You do not have enough funds to complete this policy.';

    }

  }

  if(isset($_POST['privatize'])) {

    $cost = 0;

    if($u_a[funds] >= $cost and $u_a[gdp] > 15000000) {

        $sql = ("UPDATE {$dbprefix}users SET funds = :funds, gdp = :gdp WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        $funds = $u_a[funds] + 300000;
        $gdp = $u_a[gdp] - 15000000;
        
        // Bind
        $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
        $stmt->bindParam(':gdp', $gdp, PDO::PARAM_INT);
        $stmt->execute();

        $outcome_good[] = 'Successfully privatized. $300,000 added to funds.';

    } elseif($u_a[gdp] <= 15000000) {

      $outcome_bad[] = 'Your country\'s GDP must be greater than $15,000,000.';

    } else {

      $outcome_bad[] = 'You do not have enough funds to complete this policy.';

    }

  }

  if(isset($_POST['prospect'])) {
    $cost = 500000;

    if($u_a[region] == 'middle east') {
      $maxwells = floor($u_a[territory]/6666);
    } else {
      $maxwells = floor($u_a[territory]/10000);
    }

    if($u_a[oil_production] >= $maxwells) {
      $errors[] = 'You can only have two wells per 20,000<sup>km</sup> of territory.';
    }
    if($u_a[funds] < $cost) {
      $errors[] = 'You do not have enough funds to complete this policy.';
    }

    if(!$errors) {
      $outcome = mt_rand(1, 10);
      if($outcome <= 8) {
        $sql = ("UPDATE {$dbprefix}users SET funds = :funds, has_prospected = :has_prospected, oil_production = :oil_production, oil_reserves = :oil_reserves WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);
        $funds = $u_a[funds] - $cost;
        $has_prospected = 1;
        $oil_production = $u_a[oil_production] + 1;
        $oil_reserves = $u_a[oil_reserves] + 5;

        // Bind
        $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
        $stmt->bindParam(':has_prospected', $has_prospected, PDO::PARAM_INT);
        $stmt->bindParam(':oil_production', $oil_production, PDO::PARAM_INT);
        $stmt->bindParam(':oil_reserves', $oil_reserves, PDO::PARAM_INT);
        $stmt->execute();
        $outcome_good[] = 'You strike it rich and have found 5 Mbbl of oil. You will receive 1 Mbbl (one thousand barrels) a month.';
      } elseif($outcome >= 9) {
        $sql = ("UPDATE {$dbprefix}users SET funds = :funds, has_prospected = :has_prospected WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);
        $funds = $u_a[funds] - $cost;
        $has_prospected = 1;

        // Bind
        $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
        $stmt->bindParam(':has_prospected', $has_prospected, PDO::PARAM_INT);
        $stmt->execute();
        $outcome_bad[] = 'You search endlessly but find no oil.';

      }
    }
  }

// Domestic

    if(isset($_POST['agency_name'])) {
        $agency_name = trim($_POST["agency_name"]);
        $agency_name = filter_var($agency_name, FILTER_SANITIZE_STRING);
        $cost = $growth_cost = (0.005*$u_a[gdp]) + (0.005*floor($u_a[gdp]*.000000001));
        if($u_a[funds] < $cost) {
            $errors[] = 'You do not have enough funds to complete this policy.';
        }
        if($u_a[intel_agency] != null) {
            $errors[] = 'You already have an intelligence agency.';
        }
        if(!$errors) {
            $sql = ("UPDATE {$dbprefix}users SET funds = :funds, intel_agency = :intel_agency WHERE user_id='$u_a[user_id]'");
            $stmt = $pdo->prepare($sql);

            $funds = $u_a[funds] - $cost;
            $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
            $stmt->bindParam(':intel_agency', $agency_name, PDO::PARAM_STR);
            $stmt->execute();
            $outcome_good[] = 'You create an intelligence agency.';
        }
    }

  if(isset($_POST['build_uni'])) {
    // Config
    $cost = 250000;
    if($u_a[funds] < $cost) {
      $errors[] = 'You do not have enough funds to complete this policy.';
    }
    if($u_a[growth] < 500000) {
      $errors[] = 'You do not have enough growth to complete this policy.';
    }
    if($u_a[qol] >= 10) {
      $errors[] = 'Your people are already pleased with their shanties. (Quality of Life is at maximum.)';
    }
    if(!$errors) {
      $sql = ("UPDATE {$dbprefix}users SET funds = :funds, growth = :growth, qol = :qol WHERE user_id='$u_a[user_id]'");
      $stmt = $pdo->prepare($sql);

      $funds = $u_a[funds] - $cost;
      $growth = $u_a[growth] - 250000;
      $qol = $u_a[qol] + 1;

      $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
      $stmt->bindParam(':growth', $growth, PDO::PARAM_INT);
      $stmt->bindParam(':qol', $qol, PDO::PARAM_INT);
      $stmt->execute();
      $outcome_good[] = 'You construct a university and increase the literacy and quality of life of the people of Tropico.';
    }

  }

  if(isset($_POST['convert_population'])) {
    if($u_a[population] < 7000000) {
      $errors[] = 'Your population is too low to convert to oil!';
    }
    if($u_a[reputation] < 2) {
      $errors[] = 'Your reputation is too low, the UN is itching to invade. You should let your reputation increase first.';
    }
    if($u_a[qol] <= 0) {
      $errors[] = 'Your people are too suspicious to complete this policy. (Quality of Life is too low.)';
    }
    if(!$errors) {
      $debug[] = 'Passed all checks.';

      $sql = ("UPDATE {$dbprefix}users SET population = :population, reputation = :reputation, stability = :stability, qol = :qol, rebel_threat = :rebel_threat, oil_reserves = :oil_reserves WHERE user_id='$u_a[user_id]'");
      $stmt = $pdo->prepare($sql);

      // Config
      $outcome = mt_rand(1, 10);
      $population = $u_a[population] - 100000;
      $reputation = $u_a[reputation];
      $stability = $u_a[stability];
      $qol = $u_a[qol];
      $rebel_threat = $u_a[rebel_threat];

      if($outcome <= 7) {
        $reputation = $u_a[reputation] - 1;
        $stability = $u_a[stability] - 1;
        $qol = $u_a[qol] - 1;
        $oil_reserves = $u_a[oil_reserves] + 5;

        $outcome_good[] = 'You convert a hundred thousand civilians to oil. Your reputation and stability suffer.';
      } elseif($outcome >= 8 and $outcome <= 9) {
        $reputation = $u_a[reputation] - 1;
        $stability = $u_a[stability] - 1;
        $qol = $u_a[qol] - 1;
        $rebel_threat = $u_a[rebel_threat] + 1;
        $oil_reserves = $u_a[oil_reserves] + 5;

        $outcome_bad[] = 'A hundred thousand civilians are converted to oil. Your reputation and stability suffer. Rebels notice the action and increase.';
      } elseif($outcome == 10) {
        $oil_reserves = $u_a[oil_reserves] + 5;

        $outcome_good[] = 'A hundred thousand civilians are converted to oil. No one seems to notice and your ass is safe for tonight.';
      }

      $stmt->bindParam(':population', $population, PDO::PARAM_INT);
      $stmt->bindParam(':reputation', $reputation, PDO::PARAM_INT);
      $stmt->bindParam(':stability', $stability, PDO::PARAM_INT);
      $stmt->bindParam(':qol', $qol, PDO::PARAM_INT);
      $stmt->bindParam(':rebel_threat', $rebel_threat, PDO::PARAM_INT);
      $stmt->bindParam(':oil_reserves', $oil_reserves, PDO::PARAM_INT);

      $stmt->execute();
    }
  }

  if(isset($_POST['arrest_op'])) {
    $cost = 50000;
    if($u_a[funds] < $cost) {
      $errors[] = 'You do not have enough funds to complete this policy.';
    }
    if($u_a[rebelthreat] < 1) {
      $errors[] = 'There are no rebels in your country. The policy was not attempted.';
    }

    if(!$errors) {

      if($u_a[rebelthreat] >= 1) {

        $outcome = mt_rand(1, 10);

        if($outcome <= 7) {

          $sql = ("UPDATE {$dbprefix}users SET rebelthreat = :rebelthreat, funds = :funds, govtype = :govtype WHERE user_id='$u_a[user_id]'");
          $stmt = $pdo->prepare($sql);

          $outcome = mt_rand(1, 10);

          if($outcome == 1 and $u_a[qol] < 8) {

            $govtype = 'Authoritarian Democracy';

          } else {

            $govtype = $u_a[govtype];

          }

          $amount = ( $u_a[rebelthreat] - 1 );
          $funds = $u_a[funds] - $cost;

          // Bind
          $stmt->bindParam(':govtype', $govtype, PDO::PARAM_STR);
          $stmt->bindParam(':rebelthreat', $amount, PDO::PARAM_INT);
          $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
          $stmt->execute();

          $outcome_good[] = 'You have jailed a number of revolutionaries.';

        } elseif($outcome == 8) {

          $sql = ("UPDATE {$dbprefix}users SET rebelthreat = :rebelthreat, funds = :funds WHERE user_id='$u_a[user_id]'");
          $stmt = $pdo->prepare($sql);

          $outcome = mt_rand(1, 10);

          if($outcome == 1 and $u_a[qol] < 8) {

            $govtype = 'Authoritarian Democracy';

          } else {

            $govtype = $u_a[govtype];

          }

          if($u_a[rebelthreat] <= 9) {

            $amount = ( $u_a[rebelthreat] + 1);

          } elseif($u_a[rebelthreat] == 10) {

            $amount = ( $u_a[rebelthreat] );

          }

          $funds = $u_a[funds] - $cost;

          // Bind
          $stmt->bindParam(':govtype', $govtype, PDO::PARAM_STR);
          $stmt->bindParam(':rebelthreat', $amount, PDO::PARAM_INT);
          $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
          $stmt->execute();

          $outcome_bad[] = 'The policy backfired and the rebels have grown.';

        } elseif($outcome >= 9) {

          $sql = ("UPDATE {$dbprefix}users SET funds = :funds, govtype = :govtype WHERE user_id='$u_a[user_id]'");
          $stmt = $pdo->prepare($sql);

          $outcome = mt_rand(1, 10);

          if($outcome == 1 and $u_a[qol] < 8) {

            $govtype = 'Authoritarian Democracy';

          } else {

            $govtype = $u_a[govtype];

          }

          $funds = $u_a[funds] - $cost;

          // Bind
          $stmt->bindParam(':govtype', $govtype, PDO::PARAM_STR);
          $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
          $stmt->execute();

          $outcome_neutral[] = 'You find and arrest no opposition.';

        }

      } elseif($u_a[rebelthreat] <= 0) {

        $outcome_bad[] = 'There is no rebel threat in your country.';

      }

    } else {

      $outcome_bad[] = 'You do not have enough funds to complete this policy.';

    }

  }

  if(isset($_POST['expand_territory'])) {

    $cost = 100000;

    if($u_a[funds] >= $cost) {
      $sql = ("UPDATE {$dbprefix}users SET territory = :territory, funds = :funds WHERE user_id='$u_a[user_id]'");
      $stmt = $pdo->prepare($sql);

      $outcome = mt_rand(0, 100);
      if($outcome >= 0 and $outcome <= 5) {
        $tamount = 500;
        $outcome_good[] = 'The policy is a great success. You expand your territory by 500<sup>km</sup>';
      }
      if($outcome >= 6 and $outcome <= 15) {
        $tamount = -100;
        $outcome_bad[] = 'You offend the local wildlife and lose 100<sup>km</sup> of territory.';
      }
      if($outcome >= 16 and $outcome <= 50) {
        $tamount = 0;
        $outcome_neutral[] = 'The locals do not agree with your plans of expansion. You do not lose or gain territory.';
      }
      if($outcome >= 51 and $outcome <= 100) {
        $tamount = 100;
        $outcome_good[] = 'The policy is a great success. You expand your territory by 100<sup>km</sup>';
      }

      $tamount = $u_a[territory] + $tamount;
      $funds = $u_a[funds] - $cost;

      // Bind
      $stmt->bindParam(':territory', $tamount, PDO::PARAM_INT);
      $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
      $stmt->execute();

    } else {
      $errors[] = 'You do not have enough funds to complete this policy.';
    }
  }
  if(isset($_POST['green_card'])) {
    // Config
    $cost = 300000 + $u_a[troops]*10;
    $funds = $u_a[funds] - $cost;

    if($u_a[funds] < $cost) {
      $errors[] = 'You do not have enough funds to complete this policy.';
    }
    if($u_a[manpower] >= 10) {
      $errors[] = 'Your manpower is already full.';
    }
    if(!$errors) {
      $outcome = mt_rand(1.0, 101.0);

      if($outcome <= 85.0) {
        $manpower = $u_a[manpower] + 1;
        $outcome = 'Your manpower slightly increases.';
      }
      if($outcome >= 86.0) {
        $manpower = $u_a[manpower] + 2;
        $outcome = 'Your manpower greatly increases.';
      }

      $sql = ("UPDATE {$dbprefix}users SET funds = :funds, manpower = :manpower WHERE user_id='$u_a[user_id]'");
      $stmt = $pdo->prepare($sql);
      $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
      $stmt->bindParam(':manpower', $manpower, PDO::PARAM_INT);
      $stmt->execute();

      if($stmt->rowCount() > 0) {
        $outcome_good[] = $outcome;
      }
    }
  }
  if(isset($_POST['resource_rampage'])) {
    // Config
    $cost = 100000;
    $funds = $u_a[funds] - $cost;

    if(!$errors) {
      $outcome = mt_rand(0, 5);

      if($outcome <= 0) {
        $oil_reserves = $u_a[oil_reserves];
        $outcome = 'You scour the land yet find no oil.';
      }
      if($outcome >= 1) {
        $oil_reserves = $u_a[oil_reserves] + $outcome;
        $outcome = 'You find <b>'.$outcome.'</b> Mbbl\'s of oil.';
      }

      $sql = ("UPDATE {$dbprefix}users SET funds = :funds, oil_reserves = :oil_reserves WHERE user_id='$u_a[user_id]'");
      $stmt = $pdo->prepare($sql);
      $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
      $stmt->bindParam(':oil_reserves', $oil_reserves, PDO::PARAM_INT);
      $stmt->execute();

      if($stmt->rowCount() > 0) {
        $outcome_good[] = $outcome;
      }
    }
  }

// Foreign

  if(isset($_POST['humanitarian_aid'])) {
    if($u_a[qol] >= 10) {
      $errors[] = 'Your people are already pleased with their shanties. (Quality of Life is at maximum.)';
    }
    if(!$errors) {
      $sql = ("UPDATE {$dbprefix}users SET qol = :qol, reputation = :reputation WHERE user_id='$u_a[user_id]'");
      $stmt = $pdo->prepare($sql);

      $qol = $u_a[qol] + 1;
      $reputation = $u_a[reputation] - 1;

      $stmt->bindParam(':qol', $qol, PDO::PARAM_INT);
      $stmt->bindParam(':reputation', $reputation, PDO::PARAM_INT);
      $stmt->execute();
      $outcome_neutral[] = 'Quality of life increases but your reputation with other nations declines.';
    }
  }

  if(isset($_POST['accept_refugees'])) {
    if($u_a[gov_type] == 'authoritarian democracy') {
      $errors[] = 'Authoritarian governments can not complete this policy.';
    }
    if($u_a[qol] <= 0) {
      $errors[] = 'Your Quality of Life is too low to complete this policy.';
    }
    if(!$errors) {
      $sql = ("UPDATE {$dbprefix}users SET qol = :qol, population = :population, rebel_threat = :rebel_threat WHERE user_id='$u_a[user_id]'");
      $stmt = $pdo->prepare($sql);

      $qol = $u_a[qol] - 1;
      $population = $u_a[population] + 100000;
      $rebel_threat = $u_a[rebel_threat];

      $outcome = mt_rand(1, 10);

      if($outcome <= 3) {
        $rebel_threat = $rebel_threat + 1;
        $outcome_neutral[] = 'Your population increases but quality of life decreases and rebels increase.';
      } else {
        $outcome_good[] = 'A hundred thousand refugees flock to your country. Quality of life decreases as jobs become less available.';
      }

      $stmt->bindParam(':qol', $qol, PDO::PARAM_INT);
      $stmt->bindParam(':population', $population, PDO::PARAM_INT);
      $stmt->bindParam(':rebel_threat', $rebel_threat, PDO::PARAM_INT);
      $stmt->execute();
    }

  }

  if(isset($_POST['praise_east'])) {

    $cost = 50000;

    if($u_a[funds] >= $cost and $u_a[alignment] >= -1) {

      $sql = ("UPDATE {$dbprefix}users SET funds = :funds, alignment = :alignment WHERE user_id='$u_a[user_id]'");
      $stmt = $pdo->prepare($sql);

      $funds = $u_a[funds] - $cost;
      $alignment = $u_a[alignment] - 1;

      // Bind
      $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
      $stmt->bindParam(':alignment', $alignment, PDO::PARAM_INT);
      $stmt->execute();

      $outcome_good[] = 'You have increased your standings with The East.';

    } elseif($u_a[funds] < $cost) {

      $outcome_bad[] = 'You do not have enough funds to complete this policy.';

    } elseif($u_a[alignment] <= -2) {

      $outcome_bad[] = 'You are already loyal to The East.';

    }

  }

  if(isset($_POST['praise_west'])) {

    $cost = 50000;

    if($u_a[funds] >= $cost and $u_a[alignment] <= 1) {

      $sql = ("UPDATE {$dbprefix}users SET funds = :funds, alignment = :alignment WHERE user_id='$u_a[user_id]'");
      $stmt = $pdo->prepare($sql);

      $funds = $u_a[funds] - $cost;
      $alignment = $u_a[alignment] + 1;

      // Bind
      $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
      $stmt->bindParam(':alignment', $alignment, PDO::PARAM_INT);
      $stmt->execute();

      $outcome_good[] = 'You have increased your standings with The West.';

    } elseif($u_a[funds] < $cost) {

      $outcome_bad[] = 'You do not have enough funds to complete this policy.';

    } elseif($u_a[alignment] >= 2) {

      $outcome_bad[] = 'You are already loyal to The West.';

    }

  }

// Military

  if(isset($_POST['conscript'])) {

    $cost = 0;

    if($u_a[funds] >= $cost) {

      if($u_a[manpower] >= 1) {

          $sql = ("UPDATE {$dbprefix}users SET troops = :troops, growth = :growth,  training = :training, manpower = :manpower WHERE user_id='$u_a[user_id]'");
          $stmt = $pdo->prepare($sql);

          $troops = $u_a[troops] + 2000;
          $growth = $u_a[growth] - 250000;

          if($u_a[training] >= 1) {

            $training = $u_a[training] - 1;

          } elseif($u_a[training] <= 0) {

            $training = $u_a[training];

          }

          $manpower = $u_a[manpower] - 1;

          // Bind
          $stmt->bindParam(':troops', $troops, PDO::PARAM_INT);
          $stmt->bindParam(':growth', $growth, PDO::PARAM_INT);
          $stmt->bindParam(':training', $training, PDO::PARAM_INT);
          $stmt->bindParam(':manpower', $manpower, PDO::PARAM_INT);
          $stmt->execute();

          $outcome_good[] = 'You draft thousands of young men into your military. Current Troops: '.number_format($troops).'. Your troops training level has been lowered.';

      } elseif($u_a[manpower] <= 0) {

        $outcome_bad[] = 'You do not have enough manpower.';

      }

    } else {

      $outcome_bad[] = 'You do not have enough funds to complete this policy.';

    }

  }

  if(isset($_POST['train'])) {

    $cost = $u_a[troops] * 3;

    if($u_a[funds] >= $cost) {

      if($u_a[training] < 10) {

          $sql = ("UPDATE {$dbprefix}users SET training = :training, funds = :funds WHERE user_id='$u_a[user_id]'");
          $stmt = $pdo->prepare($sql);

          $amount = ( $u_a[training] + 1 );
          $funds = $u_a[funds] - $cost;

          // Bind
          $stmt->bindParam(':training', $amount, PDO::PARAM_INT);
          $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
          $stmt->execute();

          $outcome_good[] = 'Your troops sacrifice thousands of straw dummies and increase your troops training.';

      } elseif($u_a[training] >= 10) {

        $outcome_bad[] = 'Your troops have recieved the maximum ammount of training.';

      }

    } else {

      $outcome_bad[] = 'You do not have enough funds to complete this policy.';

    }

  }

  if(isset($_POST['buy_old_bombers'])) {
    // Config
    $cost = 500000;
    if($u_a[funds] < $cost) {
      $errors[] = 'You do not have enough funds to complete this policy.';
    }
    if(!$errors) {
      $sql = ("UPDATE {$dbprefix}users SET funds = :funds, air_force = :air_force, reputation = :reputation WHERE user_id='$u_a[user_id]'");
      $stmt = $pdo->prepare($sql);

      $funds = $u_a[funds] - $cost;
      $air_force = $u_a[air_force] + 1;
      $reputation = $u_a[reputation];

      $outcome = mt_rand(1, 10);
      if($outcome <= 2) {
        $air_force = $u_a[air_force];
        $reputation = $reputation - 1;
        $outcome_bad[] = 'The bombers never arrive in country and your reputation decreases.';
      } else {
        $outcome_good[] = 'You increase your military\'s air force with old surplus bombers.';
      }

      $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
      $stmt->bindParam(':air_force', $air_force, PDO::PARAM_INT);
      $stmt->bindParam(':reputation', $reputation, PDO::PARAM_INT);
      $stmt->execute();
    }
  }
  if(isset($_POST['purchase_surplus'])) {
    // Config
    $cost = 200000;
    if($u_a[funds] < $cost) {
      $errors[] = 'You do not have enough funds to complete this policy.';
    }
    if($u_a[tech] >= 160) {
      $errors[] = 'Your equipment level is already at the maximum.';
    }
    if(!$errors) {
      $sql = ("UPDATE {$dbprefix}users SET funds = :funds, tech = :tech WHERE user_id='$u_a[user_id]'");
      $stmt = $pdo->prepare($sql);

      $funds = $u_a[funds] - $cost;
      $tech = $u_a[tech] + 1;
      $outcome_good[] = 'You have Successfully increased your equipment level. Current level: '.$tech.'.';

      $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
      $stmt->bindParam(':tech', $tech, PDO::PARAM_INT);
      $stmt->execute();
    }
  }
?>