<div class="col-md-3">

    <div class="list-group">
        <a href="?region=global" class="list-group-item <?php if ($region == "global") echo "active"?>">Global Leaders</a>
        <a href="?region=africa" class="list-group-item <?php if ($region == "africa") echo "active"?>">Africa</a>
        <a href="?region=americas" class="list-group-item <?php if ($region == "americas") echo "active"?>">Americas</a>
        <a href="?region=asia" class="list-group-item <?php if ($region == "asia") echo "active"?>">Asia</a>
        <a href="?region=europe" class="list-group-item <?php if ($region == "europe") echo "active"?>">Europe</a>
        <a href="?region=mideast" class="list-group-item <?php if ($region == "mideast") echo "active"?>">Middle East</a>
    </div>
    <div class="list-group">
        <a href="?region=west" class="list-group-item <?php if ($region == "west") echo "active"?>">Western Aligned</a>
        <a href="?region=east" class="list-group-item <?php if ($region == "east") echo "active"?>">Eastern Aligned</a>
    </div>
    <div class="list-group">
        <a href="?region=donors" class="list-group-item <?php if ($region == "donors") echo "active"?>">Donors</a>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-hover table-striped">
                <tbody>
                  <?php
                    
                    // General
                    $result = mysql_query("SELECT user_id FROM {$dbprefix}users WHERE {$whereclause} active IS NOT NULL", $link);
                    $total_acc = mysql_num_rows($result);
                    $result = mysql_query("SELECT user_id FROM {$dbprefix}users WHERE {$whereclause} active='1'", $link);
                    $total_act_acc = mysql_num_rows($result);
                    $result = mysql_query("SELECT user_id FROM {$dbprefix}users WHERE {$whereclause} active='0'", $link);
                    $total_inact_acc = mysql_num_rows($result);

                    echo 
                    '<tr><td><h6>'.
                    'Total Accounts'.
                    '</td><td><h6>'.
                    $english_format_number = number_format($total_acc).
                    '</h6></td></tr>'.
                    '<tr><td><h6>'.
                    'Total Active'.
                    '</h6></td><td><h6>'.
                    $english_format_number = number_format($total_act_acc).
                    '</td></tr>'.
                    '<tr><td><h6>'.
                    'Total Inactive'.
                    '</h6></td><td><h6>'.
                    $english_format_number = number_format($total_inact_acc).
                    '</h6></td></tr>'
                    ;

                    // Economic
                    $result = "SELECT funds FROM {$dbprefix}users WHERE {$whereclause} active='1'";
                    $query_run = mysql_query($result);

                    $totalfunds= 0;
                    while ($num = mysql_fetch_assoc ($query_run)) {

                        $totalfunds += $num['funds'];

                    }

                    $result = "SELECT growth FROM {$dbprefix}users WHERE {$whereclause} active='1'";
                    $query_run = mysql_query($result);

                    $totalgrowth= 0;
                    while ($num = mysql_fetch_assoc ($query_run)) {

                        $totalgrowth += $num['growth'];

                    }

                    $result = "SELECT gdp FROM {$dbprefix}users WHERE {$whereclause} active='1'";
                    $query_run = mysql_query($result);

                    $totalgdp= 0;
                    while ($num = mysql_fetch_assoc ($query_run)) {

                        $totalgdp += $num['gdp'];

                    }

                    echo 
                    '<tr><td><h6>'.
                    'Total Funds'.
                    '</td><td><h6>$'.
                    $english_format_number = number_format($totalfunds).
                    '</td></tr><tr><td><h6>'.
                    'Total Growth'.
                    '</td><td><h6>$'.
                    $english_format_number = number_format($totalgrowth).
                    '</td></tr><tr><td><h6>'.
                    'Total GDP'.
                    '</td><td><h6>$'.
                    $english_format_number = number_format($totalgdp)
                    ;

                    // Domestic
                    $result = "SELECT territory FROM {$dbprefix}users WHERE {$whereclause} active='1'";
                    $query_run = mysql_query($result);

                    $totalfunds= 0;
                    while ($num = mysql_fetch_assoc ($query_run)) {

                        $totalterritory += $num['territory'];

                    }

                    $result = "SELECT population FROM {$dbprefix}users WHERE {$whereclause} active='1'";
                    $query_run = mysql_query($result);

                    $totalgrowth= 0;
                    while ($num = mysql_fetch_assoc ($query_run)) {

                        $totalpopulation += $num['population'];

                    }

                    echo 
                    '<tr><td><h6>'.
                    'Total Territory'.
                    '</td><td><h6>'.
                    $english_format_number = number_format($totalterritory).
                    ' km<sup>2</sup></td></tr><tr><td><h6>'.
                    'Total Population'.
                    '</td><td><h6>'.
                    $english_format_number = number_format($totalpopulation)
                    ;

                    // Military
                    $result = "SELECT troops FROM {$dbprefix}users WHERE {$whereclause} active='1'";
                    $query_run = mysql_query($result);

                    $totaltroops= 0;
                    while ($num = mysql_fetch_assoc ($query_run)) {

                        $totaltroops += $num['troops'];

                    }

                    echo 
                    '<tr><td><h6>'.
                    'Total Troops'.
                    '</td><td><h6>'.
                    $english_format_number = number_format($totalterritory).
                    '</td></tr>'
                    ;

                  ?>
                </tbody>
            </table>
        </div>
    </div>
</div>