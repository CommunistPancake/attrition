<?php
    require_once("models/config.php");
    require_once("common/basicfunctions.php");
    require_once("common/db/link_mysql.php");
    require_once("common/db/pdo.php");
    require_once("common/userdata.php");

    $folder = $_GET['folder'];
    if($folder == "")
    {
      $folder = "events";
    } if ($folder == "events") {
        $mail_type = "mail_type='event'";
    } if ($folder == "messages") {
        $mail_type = "mail_type='pm'";
    } if ($folder == "invites") {
        $mail_type = "mail_type='invite'";
    } if ($folder == "alliance") {
        $mail_type = "mail_type='alliance_pm'";
    }

    if(!isUserLoggedIn()) {
        header("Location: index.php"); die();
    }
    if(isset($_POST['delete_event'])) {
      // Prepare the query
      $sql = "DELETE FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' AND mail_type='event'";
      $stmt = $pdo->prepare($sql);

      // Bind
      $stmt->bindParam(':user_id', $u_a[user_id], PDO::PARAM_INT);

      // Continue without errors
      if(count($errors) == 0) {   
        $stmt->execute();
        $outcome_good[] = 'Mail successfully deleted.';  
      }
    }
    if(isset($_POST['delete_pm'])) {
      // Prepare the query
      $sql = "DELETE FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' AND mail_type='pm'";
      $stmt = $pdo->prepare($sql);

      // Bind
      $stmt->bindParam(':user_id', $u_a[user_id], PDO::PARAM_INT);

      // Continue without errors
      if(count($errors) == 0) { 
        $stmt->execute();
        $outcome_good[] = 'Mail successfully deleted.';   
      }
    }
    if(isset($_POST['delete_invite'])) {
      // Prepare the query
      $sql = "DELETE FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' AND mail_type='invite'";
      $stmt = $pdo->prepare($sql);

      // Bind
      $stmt->bindParam(':user_id', $u_a[user_id], PDO::PARAM_INT);

      // Continue without errors
      if(count($errors) == 0) { 
        $stmt->execute();
        $outcome_good[] = 'Mail successfully deleted.';   
      }
    }
    if(isset($_POST['delete_alliance_pm'])) {
      // Prepare the query
      $sql = "DELETE FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' AND mail_type='alliance_pm'";
      $stmt = $pdo->prepare($sql);

      // Bind
      $stmt->bindParam(':user_id', $u_a[user_id], PDO::PARAM_INT);

      // Continue without errors
      if(count($errors) == 0) { 
        $stmt->execute();
        $outcome_good[] = 'Mail successfully deleted.';   
      }
    }
    if(isset($_POST['delete_single'])) {
        $id = trim($_POST["id"]);

        $result = mysql_query("SELECT * FROM {$dbprefix}mail WHERE mail_id='$id'", $link);
        if (!$result) {
            die('Could not query:' . mysql_error());
        }
        $mail = mysql_fetch_assoc($result);
        $num_rows = mysql_num_rows($result);

        if($num_rows == 0) {
            $errors[] = 'No mail was found with that id.';
        } elseif($mail[user_id] != $u_a[user_id]) {
            $errors[] = 'That is not your mail to delete!';
        }
        if(!$errors) {
            $sql = "DELETE FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' AND mail_id='$id'";
            $stmt = $pdo->prepare($sql);

            $stmt->execute();
            if($stmt->rowCount() > 0) {
                $outcome_good[] = 'Message successfully deleted.';   
            }
        }
    }
    if (!empty($_POST['alliance_message'])) {
        $submit = trim($_POST["alliance_message"]);
        $submit = filter_var($submit, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        if(!$submit) {
            $errors[] = 'You must enter a message.';
        }
        if(!$u_a[user_id]) {
            $errors[] = 'You must be logged in send messages.';
        }
        if(!$u_a[alliance_id]) {
            $errors[] = 'You must be in an alliance.';
        }
        if(count($errors) == 0) {
            // Get alliance info
            $aid = $u_a[alliance_id];
            require_once("common/alliancedata.php");

            // Get alliance members
            $result = mysql_query("SELECT user_id FROM {$dbprefix}users WHERE user_id!='$u_a[user_id]' AND alliance_id='$u_a[alliance_id]'", $link);
            // $result = mysql_query("SELECT user_id FROM {$dbprefix}users WHERE alliance_id='$u_a[alliance_id]'", $link);
            if (!$result) {
                die('Could not query:' . mysql_error());
            } else {
                $user = mysql_fetch_assoc($result);
                $rows = mysql_num_rows($result);

                if($rows > 0) {
                    do {
                        // echo $user[user_id].'<br>';
                        $sql = ("INSERT INTO {$dbprefix}mail (user_id, sender_id, mail_type, title, string) VALUES(:user_id, :sender_id, :mail_type, :title, :string)");
                        $stmt = $pdo->prepare($sql);

                        $mail_type = 'alliance_pm';
                        $title = 'You received a message from <a href="user.php?uid='.$u_a[user_id].'"><u>'.stripcslashes(ucwords($u_a[country_name])).'</u></a> of your alliance: <a href="alliance.php?aid='.$a_a[alliance_id].'"><u>'.stripcslashes(ucwords($a_a[alliance_name])).'</u></a>.';
                        $string = $submit;

                        $stmt->bindParam(':user_id', $user[user_id], PDO::PARAM_INT);
                        $stmt->bindParam(':sender_id', $u_a[user_id], PDO::PARAM_INT);
                        $stmt->bindParam(':mail_type', $mail_type, PDO::PARAM_STR);
                        $stmt->bindParam(':title', $title, PDO::PARAM_STR);
                        $stmt->bindParam(':string', $string, PDO::PARAM_STR);
                        $stmt->execute();
                    } while ($user = mysql_fetch_assoc($result));
                }
                if($stmt->rowCount() > 0) {
                    $outcome_good[] = 'You have successfully messaged your alliance members.';
                }
            }
        }
    }
?>

<html>
    <head>
        <title>Inbox | <?php echo $websiteName; ?></title>
    </head>

    <body>
        <? require_once("common/navigation.php"); ?>

        <div class="container">
            <div class="well">
                <? require_once("common/alerts.php"); ?>
                <div class="row">
                    <div class="col-md-4">
                        <div class="list-group">

                            <a href="?folder=events" class="list-group-item <? if($folder == 'events') echo 'active'; ?>">Events
                            <?
                                $result = mysql_query("SELECT * FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' AND mail_type='event'", $link);
                                if (!$result) {
                                    die('Could not query:' . mysql_error());
                                }
                                if (mysql_num_rows($result) >=1 ) {
                                    $num_rows = mysql_num_rows($result);
                                    echo ' <span class="badge">'.$num_rows.'</span>';
                                }
                            ?>
                            </a></li>

                            <a href="?folder=messages" class="list-group-item <? if($folder == 'messages') echo 'active'; ?>">Private Messages
                            <?
                                $result = mysql_query("SELECT * FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' AND mail_type='pm' ", $link);
                                if (!$result) {
                                    die('Could not query:' . mysql_error());
                                }
                                if (mysql_num_rows($result) >=1 ) {
                                    $num_rows = mysql_num_rows($result);
                                    echo ' <span class="badge">'.$num_rows.'</span></a></li>';
                                }
                            ?>
                            </a></li>

                            <a href="?folder=invites" class="list-group-item <? if($folder == 'invites') echo 'active'; ?>">Invites
                            <?
                                $result = mysql_query("SELECT * FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' AND mail_type='invite' ", $link);
                                if (!$result) {
                                    die('Could not query:' . mysql_error());
                                }
                                if (mysql_num_rows($result) >=1 ) {
                                    $num_rows = mysql_num_rows($result);
                                    echo ' <span class="badge">'.$num_rows.'</span></a></li>';
                                }
                            ?>
                            </a></li>

                            <a href="?folder=alliance" class="list-group-item <? if($folder == 'alliance') echo 'active'; ?>">Alliance
                            <?
                                $result = mysql_query("SELECT * FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' AND mail_type='alliance_pm' ", $link);
                                if (!$result) {
                                    die('Could not query:' . mysql_error());
                                }
                                if (mysql_num_rows($result) >=1 ) {
                                    $num_rows = mysql_num_rows($result);
                                    echo ' <span class="badge">'.$num_rows.'</span></a></li>';
                                }
                            ?>
                            </a></li>

                        </div>
                    </div>
                    <div class="col-md-8">
                        <?
                            $result = mysql_query("SELECT * FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' AND {$mail_type} ORDER BY mail_id DESC", $link);
                            if (!$result) {
                                // die('Could not query:' . mysql_error());
                                $result = mysql_query("SELECT * FROM {$dbprefix}mail WHERE user_id='$u_a[user_id]' AND mail_type='alliance_pm' ORDER BY mail_id DESC", $link);
                            }
                                $mail = mysql_fetch_assoc($result);
                                $num_rows = mysql_num_rows($result);
                            if($num_rows == 0) {
                                echo '<center><p class="text-muted">This folder is empty.</p></center>';
                            }
                            if($num_rows > 0 and $mail[mail_type] == 'event') {
                                do {
                                    echo
                                    '<form action="'.$_SERVER[PHP_SELF].'?folder='.$folder.'" method="post">
                                    <button type="button" class="close" aria-hidden="true" data-toggle="modal" data-target="#delete_single">&times;</button>
                                    <input type="hidden" name="id" value="'.$mail[mail_id].'">
                                    <div class="modal fade" id="delete_single" tabindex="-1" role="dialog" aria-labelledby="delete_single" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="delete_single">Confirm</h4>
                                          </div>
                                          <div class="modal-body">
                                            <input type="submit" class="btn btn-danger btn-block" value="Really delete this message?" name="delete_single" />
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    </form>'.
                                    '<p class="text-primary">'.
                                    stripcslashes(ucfirst($mail[title])).
                                    '</p><p class="text-muted">'.
                                    stripcslashes(ucfirst($mail[string])).
                                    '</p><p class="text-muted"><small>'.
                                    date('l, F jS, o',strtotime($mail[mail_date])).
                                    ', at '.
                                    date('g:i A T',strtotime($mail[mail_date])).
                                    '.</small></p><hr>';
                                } while ($mail = mysql_fetch_assoc($result));
                                echo '
                                    <form action="'.$_SERVER[PHP_SELF].'?folder=events" method="post">
                                    <p>
                                      <input type="submit" class="btn btn-danger btn-block" value="Delete All Events" name="delete_event">
                                    </p>
                                    </form>
                                ';
                            }
                            if($num_rows > 0 and $mail[mail_type] == 'pm') {
                                do {
                                    echo
                                    '<form action="'.$_SERVER[PHP_SELF].'?folder='.$folder.'" method="post">
                                    <button type="button" class="close" aria-hidden="true" data-toggle="modal" data-target="#delete_single">&times;</button>
                                    <input type="hidden" name="id" value="'.$mail[mail_id].'">
                                    <div class="modal fade" id="delete_single" tabindex="-1" role="dialog" aria-labelledby="delete_single" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="delete_single">Confirm</h4>
                                          </div>
                                          <div class="modal-body">
                                            <input type="submit" class="btn btn-danger btn-block" value="Really delete this message?" name="delete_single" />
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    </form>'.
                                    '<p class="text-primary">'.
                                    stripcslashes(ucfirst($mail[title])).
                                    '</p><p><blockquote>'.
                                    stripcslashes(ucfirst($mail[string])).
                                    '</blockquote></p><p class="text-muted"><small>'.
                                    date('l, F jS, o',strtotime($mail[mail_date])).
                                    ', at '.
                                    date('g:i A T',strtotime($mail[mail_date])).
                                    '.</small></p><hr>';
                                } while ($mail = mysql_fetch_assoc($result));
                                echo '
                                    <form action="'.$_SERVER[PHP_SELF].'?folder=messages" method="post">
                                    <p>
                                      <input type="submit" class="btn btn-danger btn-block" value="Delete All Messages" name="delete_pm">
                                    </p>
                                    </form>
                                ';
                            }
                            if($num_rows > 0 and $mail[mail_type] == 'invite') {
                                do {
                                    echo
                                    '<form action="'.$_SERVER[PHP_SELF].'?folder='.$folder.'" method="post">
                                    <button type="button" class="close" aria-hidden="true" data-toggle="modal" data-target="#delete_single">&times;</button>
                                    <input type="hidden" name="id" value="'.$mail[mail_id].'">
                                    <div class="modal fade" id="delete_single" tabindex="-1" role="dialog" aria-labelledby="delete_single" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="delete_single">Confirm</h4>
                                          </div>
                                          <div class="modal-body">
                                            <input type="submit" class="btn btn-danger btn-block" value="Really delete this message?" name="delete_single" />
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    </form>'.
                                    '<p class="text-primary">'.
                                    stripcslashes(ucfirst($mail[title])).
                                    '</p><p>'.
                                    stripcslashes(ucfirst($mail[string])).
                                    '</p><p class="text-muted"><small>'.
                                    date('l, F jS, o',strtotime($mail[mail_date])).
                                    ', at '.
                                    date('g:i A T',strtotime($mail[mail_date])).
                                    '.</small></p><hr>';
                                } while ($mail = mysql_fetch_assoc($result));
                                echo '
                                    <form action="'.$_SERVER[PHP_SELF].'?folder=messages" method="post">
                                    <p>
                                      <input type="submit" class="btn btn-danger btn-block" value="Delete All Messages" name="delete_invite">
                                    </p>
                                    </form>
                                ';
                            }
                            if($num_rows > 0 and $mail[mail_type] == 'alliance_pm') {
                                do {
                                    echo
                                    '<form action="'.$_SERVER[PHP_SELF].'?folder='.$folder.'" method="post">
                                    <button type="button" class="close" aria-hidden="true" data-toggle="modal" data-target="#delete_single">&times;</button>
                                    <input type="hidden" name="id" value="'.$mail[mail_id].'">
                                    <div class="modal fade" id="delete_single" tabindex="-1" role="dialog" aria-labelledby="delete_single" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="delete_single">Confirm</h4>
                                          </div>
                                          <div class="modal-body">
                                            <input type="submit" class="btn btn-danger btn-block" value="Really delete this message?" name="delete_single" />
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    </form>'.
                                    '<p class="text-primary">'.
                                    stripcslashes(ucfirst($mail[title])).
                                    '</p><p><blockquote>'.
                                    stripcslashes(ucfirst($mail[string])).
                                    '</blockquote></p><p class="text-muted"><small>'.
                                    date('l, F jS, o',strtotime($mail[mail_date])).
                                    ', at '.
                                    date('g:i A T',strtotime($mail[mail_date])).
                                    '.</small></p><hr>';
                                } while ($mail = mysql_fetch_assoc($result));
                                echo '
                                    <form action="'.$_SERVER[PHP_SELF].'?folder=alliance" method="post">
                                    <p>
                                      <input type="submit" class="btn btn-danger btn-block" value="Delete All Messages" name="delete_alliance_pm">
                                    </p>
                                    </form>
                                ';
                            }
                            if($folder == 'alliance') {
                                echo '
                                    <hr><div class="row">
                                        <div class="col-md-12">

                                            <form action="'.$_SERVER[PHP_SELF].'?folder=alliance" method="post">
                                            <p><textarea class="form-control" placeholder="Send a message to everyone in your alliance." name="alliance_message"></textarea></p>
                                            <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#alliance_pm">
                                              Send Message
                                            </button>
                                            <div class="modal fade" id="alliance_pm" tabindex="-1" role="dialog" aria-labelledby="alliance_pm" aria-hidden="true">
                                              <div class="modal-dialog">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="alliance_pm">Confirm</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    <input type="submit" class="btn btn-primary btn-block" value="Send Message" name="alliance_pm" />
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            </form>

                                        </div>
                                    </div>
                                ';
                            }
                        ?>

                    </div>
                </div>
                <? require_once("common/footer.php"); ?>
            </div>
        </div>
    </body>
</html>