<html>
    <? require_once("models/config.php"); ?>
    <head>
        <title>IRC | <? echo $websiteName; ?></title>
    </head>

    <body>
        <? require_once("common/navigation.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-md-6" align="left">
                        <h1 class="text-danger" class="page-header">
                            Join us on IRC.
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6" align="left">
                        <h6 class="text-muted"/>If you don't have an IRC client you can use a <a href="https://kiwiirc.com/client" class="text-danger">Web IRC</a> client to connect.</h6>
                    </div>
                    <div class="col-md-6" align="right">
                        <h6 class="text-muted"><p>Server Host: irc.rizon.net</p><p>Channel: #7.62</p></h6>
                    </div>
                </div>
            <? require_once("common/footer.php"); ?>
        </div>
    </body>
</html>