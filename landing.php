<?php
  	require_once("models/config.php");
  	require_once("common/basicfunctions.php");
  	if(isUserLoggedIn()) { header("Location: index.php"); die(); }
?>
<html>
	<head>
		<title><? require_once("models/config.php"); echo $websiteName; ?></title>
	</head>

	<body>
		<? require_once("common/navigation.php"); ?>

	    <div class="container">

	      	<center>
	      		<h1 class="page-header h1-landing">WELCOME TO ATTRITION.</h1>
	      		<h1 class="h3-landing">ABOUT THE GAME</h1>
	      	</center>

	      	<ul>
	      		<li>
			      	<h1 class="h3-landing">A game of attrition.</h1>
			      	<p class="p-landing">Create and control your own fictional country. Establish a world power and keep the peace with other nations, or rule with an iron fist and become a harbinger of war. Decide your country's history, and the history of the game of Attrition.</p>
			    </li><li>
			      	<h1 class="h3-landing">Text-based, responsive, anywhere.</h1>
			      	<p class="p-landing">Take your nation anywhere you go. Attrition is built upon the bootstrap framework for it's ease of use and responsive design to be played anywhere on almost any device. </p>
			    </li><li>
			      	<h1 class="h3-landing">Community feedback.</h1>
			      	<p class="p-landing">Your feedback is valuable to the development and success of Attrition. We use an <a href="https://bitbucket.org/sven62/attrition/issues?status=new&status=open">issue tracker</a> to acknowledge bugs and player feedback.</p>
			    </li><li>
			      	<h1 class="h3-landing">Fair standards.</h1>
			      	<p class="p-landing">We accept donations but do not grant any advantages to players.</p>
			    </li>
	      	</ul>

		    <? require_once("common/footer.php"); ?>
	    </div>
	</body>
</html>