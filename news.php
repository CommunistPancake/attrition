<?php
    require_once("models/config.php");
    require_once("common/db/link_mysql.php");
    require_once("common/basicfunctions.php");
    require_once("common/userdata.php");
    require_once("common/db/pdo.php");

    if (!empty($_POST['submit_news'])) {
        $submit = trim($_POST["submit_news"]);
        $submit = filter_var($submit, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        if(!$submit) {
            $errors[] = 'You must enter a message.';
        }
        if($u_a[is_admin] == false) {
            if($u_a[has_posted_news] >= 1) {
                $errors[] = 'You may only submit news once a turn.';
            }
        }
        if(!$u_a[user_id]) {
            $errors[] = 'You must be logged in to submit news.';
        }

        if(count($errors) == 0) {
            $sql = ("INSERT INTO {$dbprefix}news (sender_id, message) VALUES (:sender_id, :message)");
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':sender_id', $u_a[user_id], PDO::PARAM_INT);
            $stmt->bindParam(':message', $submit, PDO::PARAM_STR);
            $stmt->execute();

            if($stmt->rowCount() > 0) {
                $outcome_good[] = 'The news has been successfully posted.';

                $sql = ("UPDATE {$dbprefix}users SET has_posted_news = :has_posted_news WHERE user_id='$u_a[user_id]'");
                $stmt = $pdo->prepare($sql);

                $has_posted_news = 1;

                $stmt->bindParam(':has_posted_news', $has_posted_news, PDO::PARAM_INT);
                $stmt->execute();
            } else {
                $errors[] = 'News message creation failed. (No data entered)';
            }


        }
    }
?>
<html>
    <head>
        <title>News | <?php echo $websiteName; ?></title>
    </head>

    <body>
        <? require_once("common/navigation.php"); ?>

        <div class="container">
            <div class="well">
                <?
                    require_once("common/alerts.php");

                    $result = mysql_query("SELECT * FROM {$dbprefix}economy WHERE commodity='oil'", $link);
                    $result = mysql_fetch_assoc($result);
                ?>

                <center><h2>Current price per Mbbl: <? echo number_format($result[var1]); ?></h2></center>

                <hr><div class="row">
                    <div class="col-md-12">

                          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
                            <p><textarea class="form-control input-sm" placeholder="Use this to send hot and steamy messages to your allies or call your enemies names." name="submit_news"></textarea></p>
                            <p><input type="submit" class="btn btn-primary btn-xs" value="Submit News"/></p>
                          </form>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-body">

                                <?php

                                    $limit = 10;

                                    if(isset($_GET["page"]) and $_GET["page"] == 0 ) {

                                        $page  = 1;

                                    } elseif(isset($_GET["page"])) {

                                        $page  = $_GET["page"];

                                    } else {

                                        $page=1;

                                    };  

                                    $start_from = ($page-1) * $limit;  

                                    // Retrieve data
                                    $news_result = mysql_query("SELECT * FROM attr_news ORDER BY event_id DESC LIMIT $start_from, $limit", $link);

                                    $row = mysql_fetch_assoc($news_result);
                                    $num_rows = mysql_num_rows($news_result);

                                    // Display data
                                    if($num_rows > 0) { ?>
                                    
                                    <table class="table table-hover table-striped">

                                            <thead>
                                                <tr>
                                                    <th style="width: 20%" class="text-muted">Flag</th>
                                                    <th class="text-muted">EID</th>
                                                    <th class="text-muted">Sender</th>
                                                    <th class="text-muted">Message</th>
                                                    <th class="text-muted">Date</th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                            <? do {
                                                $result = mysql_query("SELECT * FROM attr_users WHERE user_id='$row[sender_id]'", $link);
                                                $user_res = mysql_fetch_assoc($result);
                                            ?>

                                                <tr>
                                                    <td>
                                                        <?
                                                            if($user_res[custom_flag] and $u_a[safe_mode] == 0) {
                                                                echo '<a href="user.php?uid='.$user_res[user_id].'"><img style="width: 100%;" src="'.$user_res[custom_flag].'"></a>';
                                                            } else {
                                                                echo getflagfile($user_res[country_flag]);
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <h5 class="text-muted"><? echo $row[event_id] ?></h5>
                                                    </td>
                                                    <td>
                                                        <h5><a href="user.php?uid=<? echo $user_res[user_id]; ?>"><? echo stripcslashes(ucwords($user_res[country_name])) ?></a></h5>
                                                    </td>
                                                    <td>
                                                        <h5 class="text-muted"><? echo stripcslashes(ucfirst($row[message])) ?></h5>
                                                    </td>
                                                    <td>
                                                        <h5 class="text-muted"><? echo date('m/d/y', strtotime($row['date'])).' at '.date('g:i A T',strtotime($row['date'])) ?></h5>
                                                    </td>
                                                </tr>

                                            <? } while ($row = mysql_fetch_assoc($news_result));

                                        }

                                        ?>

                                        </tbody>
                                    </table>

                                <center>
                                    <?php  

                                        $result = mysql_query("SELECT COUNT(*) FROM attr_news", $link);
                                        $row = mysql_fetch_row($result);

                                        $total_records = $row[0];  
                                        $total_pages = ceil($total_records / $limit); 
                                        $pagLink = '<ul class="pagination">';

                                        for ($i=1; $i<=$total_pages; $i++) {  
                                                     $pagLink .= "<li><a href='news.php?page=".$i."''>".$i."</a></li>";  
                                        };

                                        echo $pagLink . '</ul>';  
                                    ?>
                                </center>

                            </div>
                        </div>
                    </div>
                </div> 
                <? require_once("common/footer.php"); ?>
            </div>
        </div>
    </body>
</html>