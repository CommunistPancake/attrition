<?
    require_once("models/config.php");
    require_once("common/basicfunctions.php");
    require_once("common/userfunctions.php");
    require_once("common/userdata.php");

    if(!isUserLoggedIn()) {
        header("Location: index.php"); die();
    }
    if (!empty($_POST['country_name'])) {
        $sql = ("UPDATE {$dbprefix}users SET country_name = :country_name WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        // Validate
        $country_name = trim($_POST["country_name"]);
        $country_name = filter_var($country_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        echo country_namecheck($country_name);

        if(!$country_name) {
          $errors = 'You must enter a country name.';
        }
        if(minMaxRange(3,35,$country_name))
        {
          $errors[] = lang("ACCOUNT_COUNTRY_CHAR_LIMIT",array(3,35));
        }

        // Bind
        $stmt->bindParam(':country_name', $_POST['country_name'], PDO::PARAM_STR);
        if(count($errors) == 0) {
            $old_name = $u_a[country_name];

            $stmt->execute();
            $success[] = 'Country name successfully updated.';

            $sql = "INSERT INTO {$dbprefix}events (event_type,event_action) VALUES (:event_type,:event_action)";
            $stmt = $pdo->prepare($sql);

            // Config
            $event_type = 'general';
            $event_action = '<a href="user.php?uid='.$u_a[user_id].'">'.stripcslashes(ucwords($old_name)).'</a> is now known as '.'<a href="user.php?uid='.$u_a[user_id].'">'.stripcslashes(ucwords($country_name)).'</a>';

            $stmt->bindParam(':event_type', $event_type, PDO::PARAM_STR);
            $stmt->bindParam(':event_action', $event_action, PDO::PARAM_STR);
            // $stmt->bindParam(':attacker_id', $u_a[user_id], PDO::PARAM_INT);
            $stmt->execute();
        }
    }
    if (!empty($_POST['description'])) {
        $sql = ("UPDATE {$dbprefix}users SET description = :description WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        // Validate
        $description = trim($_POST["description"]);
        $description = filter_var($description, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        // Bind
        $stmt->bindParam(':description', $_POST['description'], PDO::PARAM_STR);

        if(!$description) {
            $errors = 'You must enter a description.';
        }
        if(minMaxRange(3,256,$description)) {
            $errors[] = lang("ACCOUNT_DESCRIPTION_CHAR_LIMIT",array(3,256));
        }
        if(count($errors) == 0) {
            $stmt->execute();
            $success[] = 'Description successfully updated.';
        }
    }
    if (!empty($_POST['custom_title'])) {
        // if($u_a[donor] != true) {
        //     $errors[] = 'You must be a donor to set a custom title';
        // }
        if(count($errors) == 0) { 
            $sql = ("UPDATE {$dbprefix}users SET custom_title = :custom_title WHERE user_id='$u_a[user_id]'");
            $stmt = $pdo->prepare($sql);

            $custom_title = sanitize(trim($_POST["custom_title"]));

            // Bind
            $stmt->bindParam(':custom_title', $_POST['custom_title'], PDO::PARAM_STR);

            if(!$custom_title) {
              $errors[] = 'You must enter a custom title.';
            }
            if(count($errors) == 0) { 
              $stmt->execute();
              $success[] = 'Custom title successfully updated.'; 
            }
        }
    }
    if (!empty($_POST['anthem_url'])) {
        $sql = ("UPDATE {$dbprefix}users SET anthem_url = :anthem_url WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        // Validate
       $anthem_url = trim($_POST["anthem_url"]);
       $andthem_url = filter_var($anthem_url, FILTER_SANITIZE_URL);

        // Bind
        $stmt->bindParam(':anthem_url', $anthem_url, PDO::PARAM_STR);

        if(!$anthem_url) {
            $errors = 'You must enter an anthem url.';
        }
        if(minMaxRange(3,150,$anthem_url)) {
            $errors[] = 'The anthem url code must be between 3 and 150 characters.';
        }
        if(count($errors) == 0) {
            $stmt->execute();
            $success[] = 'Country anthem successfully updated.';
        }
    }
    if (!empty($_POST['country_flag'])) {
        $sql = ("UPDATE {$dbprefix}users SET country_flag = :country_flag WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        $country_flag = trim($_POST["country_flag"]);
        $country_flag = filter_var($country_flag, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        // Bind
        $stmt->bindParam(':country_flag', $country_flag, PDO::PARAM_STR);

        // Validate
        $country_flag = trim($_POST["country_flag"]);
        if(!$country_flag) {
          $errors[] = lang("ACCOUNT_COUNTRY_NO_FLAG");
        }
        if(count($errors) == 0) { 
            $stmt->execute();
            $success[] = 'Flag successfully updated.';
        }
    }
    if (!empty($_POST['custom_flag'])) {
        // if($u_a[donor] != true) {
        //     $errors[] = 'You must be a donor to set a custom flag';
        // }
        if(count($errors) == 0) { 
            $sql = ("UPDATE {$dbprefix}users SET custom_flag = :custom_flag WHERE user_id='$u_a[user_id]'");
            $stmt = $pdo->prepare($sql);

            $custom_flag = trim($_POST["custom_flag"]);
            $custom_flag = filter_var($custom_flag, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

            // Bind
            $stmt->bindParam(':custom_flag', $custom_flag, PDO::PARAM_STR);

            if(!$custom_flag) {
              $errors[] = lang("ACCOUNT_COUNTRY_NO_FLAG");
            }
            if(count($errors) == 0) { 
                $stmt->execute();
                $success[] = 'Custom flag successfully updated.'; 
            }
        }
    }
    if (!empty($_POST['country_leader'])) {
        $sql = ("UPDATE {$dbprefix}users SET country_leader = :country_leader WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        $country_leader = trim($_POST["country_leader"]);
        $country_leader = filter_var($country_leader, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        // Bind
        $stmt->bindParam(':country_leader', $country_leader, PDO::PARAM_STR);

        if(!$country_leader) {
            $errors[] = lang("ACCOUNT_COUNTRY_NO_LEADER");
        }
        if(count($errors) == 0) {
            $stmt->execute();
            $success[] = 'Leader successfully updated.';
        }
    }
    if (!empty($_POST['custom_leader'])) {
        // if($u_a[donor] != true) {
        //     $errors[] = 'You must be a donor to set a custom leader';
        // }
        if(count($errors) == 0) { 
            $sql = ("UPDATE {$dbprefix}users SET custom_leader = :custom_leader WHERE user_id='$u_a[user_id]'");
            $stmt = $pdo->prepare($sql);

            $custom_leader = trim($_POST["custom_leader"]);
            $custom_leader = filter_var($custom_leader, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

            // Bind
            $stmt->bindParam(':custom_leader', $custom_leader, PDO::PARAM_STR);

            if(!$custom_leader) {
              $errors[] = lang("ACCOUNT_COUNTRY_NO_FLAG");
            }
            if(count($errors) == 0) { 
                $stmt->execute();
                $success[] = 'Custom leader successfully updated.';
            }
        }
    }
    if (!empty($_POST['safe_mode'])) {
        $sql = ("UPDATE {$dbprefix}users SET safe_mode = :safe_mode WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        $safe_mode = trim($_POST["safe_mode"]);
        $safe_mode = filter_var($safe_mode, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        // Bind
        $stmt->bindParam(':safe_mode', $safe_mode, PDO::PARAM_INT);

        if(!$safe_mode) {
            $errors[] = lang("ACCOUNT_COUNTRY_NO_SAFE_MODE");
        }

        // Continue without errors
        if(count($errors) == 0) {
            $stmt->execute();
            $success[] = 'Safe mode successfully updated.'; 
        }  
    }
    if (!empty($_POST['theme'])) {
        $sql = ("UPDATE {$dbprefix}users SET theme = :theme WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        $theme = trim($_POST["theme"]);
        $theme = filter_var($theme, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        // Bind
        $stmt->bindParam(':theme', $theme, PDO::PARAM_STR);

        if(!$theme) {
            $errors[] = lang("ACCOUNT_COUNTRY_NO_SAFE_MODE");
        }
        // Continue without errors
        if(count($errors) == 0) { 
            $stmt->execute();
            $success[] = 'Theme successfully updated.';
        }
    }
    if (!empty($_POST['overwrite_theme'])) {
        $sql = ("UPDATE {$dbprefix}users SET overwrite_theme = :overwrite_theme WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        $overwrite_theme = trim($_POST["overwrite_theme"]);
        $overwrite_theme = filter_var($overwrite_theme, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        // Bind
        $stmt->bindParam(':overwrite_theme', $overwrite_theme, PDO::PARAM_INT);

        if(!$overwrite_theme) {
            $errors[] = lang("ACCOUNT_COUNTRY_NO_SAFE_MODE");
        }
        if(count($errors) == 0) {
            $stmt->execute();
            $success[] = 'Overwrite successfully updated.'; 
        }   
    }
    if (!empty($_POST['icons_enabled'])) {
        $sql = ("UPDATE {$dbprefix}users SET icons_enabled = :icons_enabled WHERE user_id='$u_a[user_id]'");
        $stmt = $pdo->prepare($sql);

        $bool = trim($_POST["icons_enabled"]);
        $bool = filter_var($bool, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        // Bind
        $stmt->bindParam(':icons_enabled', $bool, PDO::PARAM_INT);

        // Validate
        $icons_enabled = trim($_POST["icons_enabled"]);
        if(!$icons_enabled) {
            $errors[] = 'You must select an icon preference.';
        }
        if(count($errors) == 0) {
            $stmt->execute();
            $success[] = 'Icon preference successfully updated.'; 
        }   
    }
    require("common/userdata.php");
?>

<html>
    <head>
        <title>Preferences | <?php echo $websiteName; ?></title>
    </head>

    <body>
        <? require_once("common/navigation.php"); ?>

        <div class="container">
            <div class="well">
                <? require_once("common/alerts.php"); ?>

                <div class="row">
                    <div class="col-md-6">
                        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                            <p><input type="text" class="form-control input-sm" placeholder="Country Name" name="country_name" maxlength="20"></p>
                    </div>
                    <div class="col-md-6">  
                        <?php echo '<p class="text-muted">' . stripcslashes(ucwords($u_a[country_name])) . '</p>' ?>
                    </div>
                </div>

                <hr><div class="row">
                    <div class="col-md-6">
                        <p><textarea class="form-control input-sm" placeholder="Country Description" name="description" maxlength="150"></textarea></p>
                    </div>
                    <div class="col-md-6">
                        <?
                            if($u_a[description] == null) {
                                if($u_a[user_id] == $u_a[user_id]) {
                                    echo '<p class="text-muted">Welcome to Attrition. This is the default country description. To change your description visit the <a href="preferences.php">settings</a> page.</p>';
                                } else {
                                    echo '<p class="text-muted">This country\'s leader has not set their description yet. <em>The country might be an easy target for an attack.</em></p>';
                                }
                            } else {
                                echo '<p class="text-muted">' . stripcslashes(ucfirst($u_a[description])) . '</p>';
                            }
                        ?>
                    </div>
                </div>

                <hr><div class="row">
                    <div class="col-md-6">
                        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                            <p><input type="text" class="form-control input-sm" placeholder="Custom Leader Title" name="custom_title" maxlength="30"></p>
                    </div>
                    <div class="col-md-6">  
                        <?php echo '<p class="text-muted">' . stripcslashes(ucfirst($u_a[custom_title])) . '</p>' ?>
                    </div>
                    <div class="col-md-6">
                        <?
                            if($u_a[custom_title]) {
                               echo '<p class="text-muted">Custom Title: '.stripcslashes(ucfirst($u_a[custom_title])).'</p>';
                            } else {
                                echo '<p class="text-muted">Custom Title: Not set.</p>';
                            }
                        ?>
                    </div>
                </div>

                <hr><div class="row">
                    <div class="col-md-6">
                        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                            <p><input type="text" class="form-control input-sm" placeholder="Country Anthem URL" name="anthem_url" maxlength="20"></p>
                    </div>
                    <div class="col-md-6">
                        <?
                            if($u_a[anthem_url]) {
                               echo '<p class="text-muted">Current URL: <a href="https://www.youtube.com/watch?v=' . $u_a[anthem_url] . '" target="_blank">'. $u_a[anthem_url] .'</a></p>';
                            } else {
                                echo '<p class="text-muted">Current URL: Not set.</p>';
                            }
                        ?>
                        <p class="text-muted"><small>This will only work with a video id from youtube. For example: <a href="https://www.youtube.com/watch?v=c4wHJqqud3U" target="_blank">c4wHJqqud3U</a></small></p>
                    </div>
                </div>

                <hr><div class="row">
                    <div class="col-md-6">
                        <select class="form-control input-sm" name="country_flag">
                            <option value="" disabled selected style='display:none;'>Country Flag</option>
                            <? require_once("common/flagoptions.php"); ?>
                        </select>
                    </div>
                    <div class="col-md-6">              
                        <?php if($u_a[country_flag] != null) { echo '<div style="width: 50%;">'.getflagfile($u_a[country_flag]).'</div>'; } ?>
                  </div>
                </div>

                <hr><div class="row">
                    <div class="col-md-6">
                        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                            <p><input type="text" class="form-control input-sm" placeholder="Custom Flag URL" name="custom_flag" maxlength="150"></p>
                    </div>
                    <div class="col-md-6">
                        <?
                            if($u_a[custom_flag]) {
                               echo '<img style="width: 50%;" src="'.$u_a[custom_flag].'"><p class="text-muted">Current URL: <a href="' . $u_a[custom_flag] . '" target="_blank">'. $u_a[custom_flag] .'</a></p>';
                            } else {
                                echo '<p class="text-muted">Current URL: Not set.</p>';
                            }
                        ?>
                        <p class="text-muted"><small>Make sure to include the entire url.</small></p>
                    </div>
                </div>

                <hr><div class="row">
                    <div class="col-md-6">
                        <select class="form-control input-sm" name="country_leader">
                            <option value="" disabled selected style='display:none;'>Country Leader</option>
                            <option value="default">Default</option>
                            <option value="lenin">Lenin</option>
                            <option value="hitler">Hitler</option>
                            <option value="fudd">Fudd</option>
                            <option value="eisenhower">Eisenhower</option>
                            <option value="feels_brit">Feels (British Army)</option>
                            <option value="feels_german">Feels (Wehrmacht)</option>
                            <option value="ford">Gerald Ford</option>
                            <option value="gh_bush">George H. W. Bush</option>
                            <option value="gw_bush">George W. Bush</option>
                            <option value="gorbachev">Mikhail Gorbachev</option>
                            <option value="hussein">Saddam Hussein</option>
                            <option value="ian_smith">Ian Smith</option>
                            <option value="kebab">Serbia Strong</option>
                            <option value="mao_zedong">Mao Zedong</option>
                            <option value="takeo_miki">Takeo Miki</option>
                            <option value="lee_flag">Robert E. Lee (Flags)</option>
                            <option value="lee_portrait">Robert E. Lee (Portrait)</option>
                        </select>
                    </div>
                    <div class="col-md-6">               
                        <?php if($u_a[country_leader] != null) { echo '<img class="media-object" style="width: 128px;" style="width: 100%;" src="'.getleaderfile($u_a[country_leader]).'">'; } ?>
                    </div>
                </div>

                <hr><div class="row">
                    <div class="col-md-6">
                        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                            <p><input type="text" class="form-control input-sm" placeholder="Custom Leader URL" name="custom_leader" maxlength="150"></p>
                    </div>
                    <div class="col-md-6">
                        <?
                            if($u_a[custom_leader]) {
                               echo '<img style="width: 50%;" src="'.$u_a[custom_leader].'"><p class="text-muted">Current URL: <a href="' . $u_a[custom_leader] . '" target="_blank">'. $u_a[custom_leader] .'</a></p>';
                            } else {
                                echo '<p class="text-muted">Current URL: Not set.</p>';
                            }
                        ?>
                        <p class="text-muted"><small>Make sure to include the entire url.</small></p>
                    </div>
                </div>

                <!-- SFW Mode -->
                <hr><div class="row">
                    <div class="col-md-6">
                        <select class="form-control input-sm" name="safe_mode">
                            <option value="" disabled selected style='display:none;'>SFW Mode</option>
                            <option value="1">Safe For Work</option>
                            <option value="false">Not Safe For Work</option>
                      </select>
                    </div>
                    <div class="col-md-6">
                        <?
                            if($u_a[safe_mode] == true) {
                                echo '
                                <span class="text-muted">Current Mode:</span> <a class="text-success" data-toggle="tooltip" id="sfw" title="NSFW content will be removed.">Safe For Work.</a>
                                ';
                            } elseif($u_a[safe_mode] == false) {
                                echo '
                                <span class="text-muted">Current Mode:</span> <a class="text-danger" data-toggle="tooltip" id="nsfw" title="NSFW content will not be removed.">Not Safe For Work.</a>
                                ';
                            }
                        ?>
                        <script type="text/javascript">
                            $('#sfw').tooltip();
                            $('#nsfw').tooltip();
                        </script>
                    </div>
                </div>

                <!-- Theme -->
                <hr><div class="row">
                    <div class="col-md-6">
                        <select class="form-control input-sm" name="theme">
                            <option value="" disabled selected style='display:none;'>Theme</option>
                            <option value="default">Bootstrap</option>
                            <option value="amelia">Amelia</option>
                            <option value="cerulean">Cerulean</option>
                            <option value="cirrus">Cirrus</option>
                            <option value="cosmo">Cosmo</option>
                            <option value="cyborg">Cyborg</option>
                            <option value="flatly">Flatly</option>
                            <option value="geo">Geo</option>
                            <option value="journal">Journal</option>
                            <option value="readable">Readable</option>
                            <option value="simplex">Simplex</option>
                            <option value="slate">Slate</option>
                            <option value="spacelab">Spacelab</option>
                            <option value="united">United</option>
                            <option value="white_plum">White Plum</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <?
                        echo '<span class="text-muted">Current Theme:</span> <a data-toggle="tooltip" id="theme" title="This changes theme on all pages for the game. Other players will also be able to see the theme you are using when visiting your profile.">' . ucfirst($u_a[theme]) . '.</a>'
                        ?>
                        <script type="text/javascript">
                            $('#theme').tooltip();
                        </script>

                    </div>
                </div>

                <!-- Allow Theme Overwrite -->
                <hr><div class="row">
                    <div class="col-md-6">
                        <select class="form-control input-sm" name="overwrite_theme">
                            <option value="" disabled selected style='display:none;'>Allow Theme Overwrite</option>
                            <option value="1">Enable</option>
                            <option value="false">Disable</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <span class="text-muted">Overwrite Mode:</span>
                        <?
                            if($u_a[overwrite_theme] == true) {
                                echo '<a class="text-success" data-toggle="tooltip" id="overwrite_theme" title="This changes theme on other user\'s profiles based on their selected theme.">Enabled.</a>';
                            } else {
                                echo '<a class="text-danger" data-toggle="tooltip" id="overwrite_theme" title="This changes theme on other user\'s profiles based on their selected theme.">Disabled.</a>';
                            }
                        ?>
                        <script type="text/javascript">
                            $('#overwrite_theme').tooltip();
                        </script>
                    </div>
                </div>

                <!-- Icons enabled -->
                <hr><div class="row">
                    <div class="col-md-6">
                        <select class="form-control input-sm" name="icons_enabled">
                            <option value="" disabled selected style='display:none;'>Enable Icons</option>
                            <option value="1">Enable</option>
                            <option value="false">Disable</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <span class="text-muted">Icons:</span>
                        <?
                            if($u_a[icons_enabled] == true) {
                                echo '<a class="text-success">Enabled.</a>';
                            } else {
                                echo '<a class="text-danger">Disabled.</a>';
                            }
                        ?>
                    </div>
                </div>
                <!-- Input length -->
                <script type="text/javascript">
                  $('input.form-control').maxlength({
                        alwaysShow: true,
                        // threshold: 10,
                        warningClass: "label label-info",
                        limitReachedClass: "label label-s"
                    });
                </script>

                <hr><div class="row">
                    <div class="col-md-12">
                        <input type="submit" class="btn btn-primary btn-block" value="Update Preferences" name="update_preferences" />
                        </form>
                    </div>
                </div>

                <? require_once("common/footer.php"); ?>
            </div>
        </div>
    </body>
</html>