<?php
  	require_once("models/config.php");
  	require_once("common/basicfunctions.php");
  	require_once("common/userfunctions.php");
  	if(isUserLoggedIn()) { header("Location: index.php"); die(); }

	if(!empty($_POST)) {

		$errors = array();
		$email = trim($_POST["email"]);
		$username = trim($_POST["username"]);
		$password = trim($_POST["password"]);
		$confirm_pass = trim($_POST["passwordc"]);
		$country_name = trim($_POST["countryname"]);
		$gov_type = trim($_POST["govtype"]);
		$eco_type = trim($_POST["ecotype"]);
		$country_flag = trim($_POST["country_flag"]);
		$region = trim($_POST["region"]);
		$ip_address = $_SERVER['REMOTE_ADDR'];

		echo country_namecheck($country_name);

		function checkemailservice($email) {
			global $errors;
			$disallowed_email_services = array('drdrb.com', 'sharklasers.com', 'mailinator.com', 'guerrillamail.com');
			while (list(, $service) = each($disallowed_email_services)) {
				if(strpos($email, $service) !== false) {
					$errors[] = 'Disallowed email address service.';
				}
			}
		}
		echo checkemailservice($email);

		// Check that the values are valid
		$valid_gov_types = array('Authoritarian Democracy','Dictatorship','Liberal Democracy','Military Junta','One Party Rule');
		if (!in_array($gov_type, $valid_gov_types)) {
			$errors[] = 'Invalid government system.';
		}
		$valid_eco_types = array('Free Market','Mixed Economy','Central Planning');
		if (!in_array($eco_type, $valid_eco_types)) {
			$errors[] = 'Invalid economic system.';
		}
		$valid_regions = array('Africa','Americas','Asia','Europe','Middle East');
		if (!in_array($region, $valid_regions)) {
			$errors[] = 'Invalid country region.';
		}

		if(minMaxRange(3,20,$username))
		{
			$errors[] = lang("ACCOUNT_USER_CHAR_LIMIT",array(3,20));
		}
		if(minMaxRange(5,50,$password) && minMaxRange(5,50,$confirm_pass))
		{
			$errors[] = lang("ACCOUNT_PASS_CHAR_LIMIT",array(5,50));
		}
		else if($password != $confirm_pass)
		{
			$errors[] = lang("ACCOUNT_PASS_MISMATCH");
		}
		if(!isValidemail($email))
		{
			$errors[] = lang("ACCOUNT_INVALID_EMAIL");
		}

		// Country name length
		if(minMaxRange(3,20,$country_name))
		{
			$errors[] = lang("ACCOUNT_COUNTRY_CHAR_LIMIT",array(3,20));
		}
		// Govt type
		if(!$gov_type) {
			$errors[] = lang("ACCOUNT_COUNTRY_NO_GOV_TYPE");
		}
		// Countryflag
		if(!$country_flag) {
			$errors[] = lang("ACCOUNT_COUNTRY_NO_FLAG");
		}
		// Region
		if(!$region) {
			$errors[] = lang("ACCOUNT_COUNTRY_NO_REGION");
		}

		require_once('common/external/recaptchalib.php');
		$privatekey = "6Lcul-sSAAAAACTvH_0rgJOxR20AgLOZmZnhnRVJ";
		$resp = recaptcha_check_answer ($privatekey,
		                            $_SERVER["REMOTE_ADDR"],
		                            $_POST["recaptcha_challenge_field"],
		                            $_POST["recaptcha_response_field"]);

		if (!$resp->is_valid) {
			$errors[] = ("The reCAPTCHA wasn't entered correctly. Go back and try it again."."(reCAPTCHA said: " . $resp->error . ")");
		}

		//End data validation
		if(count($errors) == 0)
		{	
				//Construct a user object
				$user = new User($username,$password,$email,$country_name,$gov_type,$eco_type,$country_flag,$region,$ip_address);
				
				//Checking this flag tells us whether there were any errors such as possible data duplication occured
				if(!$user->status)
				{
					if($user->username_taken) $errors[] = lang("ACCOUNT_USERNAME_IN_USE",array($username));
					if($user->email_taken) $errors[] = lang("ACCOUNT_EMAIL_IN_USE",array($email));	
				}
				else
				{
					//Attempt to add the user to the database, carry out finishing  tasks like emailing the user (if required)
					if(!$user->userPieAddUser())
					{
						if($user->mail_failure) $errors[] = lang("MAIL_ERROR");
						if($user->sql_failure)  $errors[] = lang("SQL_ERROR");
					}
				}
		}
	   if(count($errors) == 0) 
	   {
		        if($emailActivation)
		        {
		             $message[] = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE2");
		        } else {
		             $message[] = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE1");
		        }
	   }
	}
?>
<html>
	<head>
  		<title>Register | <?php echo $websiteName; ?></title>
	</head>

	<body>
		<? require_once("common/navigation.php"); ?>

	  		<div class="container">
	    		<div class="well">
	    			<? require_once("common/alerts.php"); ?>

					<script type="text/javascript">
						$('input.form-control').maxlength({
					        alwaysShow: true,
					        // threshold: 10,
					        warningClass: "label label-success",
					        limitReachedClass: "label label-s"
					    });
					</script>

	    			<div class="row">
	    				<div class="col-md-6">
						<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">

							<h5>Account Information</h5>

							<p><input type="text" name="username" class="form-control" placeholder="Username" maxlength="20"
							<? if($errors) { echo 'value="'.$username.'"'; } ?>
							></p>
							<p><input type="password" name="password" class="form-control" placeholder="Password" maxlength="20"></p>
							<p><input type="password" name="passwordc" class="form-control" placeholder="Confirm Password" maxlength="20"></p>
							<p><input type="text" name="email" class="form-control" placeholder="Email Address"
							<? if($errors) { echo 'value="'.$email.'"'; } ?>
							></p>

							<hr>
							<h5>Your New Country</h5>

							<p><input type="text" name="countryname" class="form-control" placeholder="Country Name" maxlength="20"<? if($errors) { echo 'value="'.$country_name.'"'; } ?>
							></p>

							<p><select class="form-control" name="govtype" class="form-control"></p>
							<option value="" disabled selected style='display:none;'>Government System</option>
							<? if($errors) { echo '<option selected value="'.$gov_type.'">'.ucfirst($gov_type).'</option>'; } ?>
							<option value="Authoritarian Democracy">Authoritarian Democracy</option>
							<option value="Dictatorship">Dictatorship</option>
							<option value="Liberal Democracy">Liberal Democracy</option>
							<option value="Military Junta">Military Junta</option>
							<option value="One Party Rule">One Party Rule</option>
							</select>

							<p><select class="form-control" name="ecotype" class="form-control"></p>
							<option value="" disabled selected style='display:none;'>Economic System</option>
							<? if($errors) { echo '<option selected value="'.$eco_type.'">'.ucfirst($eco_type).'</option>'; } ?>
							<option value="Free Market">Free Market</option>
							<option value="Mixed Economy">Mixed Economy</option>
							<option value="Central Planning">Central Planning</option>
							</select>

							<p><select class="form-control" name="country_flag"></p>
							<? require_once("common/flagoptions.php") ?>
							<? if($errors) { echo '<option selected value="'.$country_flag.'">'.ucfirst($country_flag).'</option>'; } ?>
							</select>
						
							<p><select class="form-control" name="region" class="form-control"></p>
							<option value="" disabled selected style='display:none;'>Country Region</option>
							<? if($errors) { echo '<option selected value="'.$region.'">'.ucfirst($region).'</option>'; } ?>
							<option value="Africa">Africa</option>
							<option value="Americas">Americas</option>
							<option value="Asia">Asia</option>
							<option value="Europe">Europe</option>
							<option value="Middle East">Middle East</option>
							</select>

							<hr>

							<p>
							<?
								require_once('common/external/recaptchalib.php');
								$publickey = "6Lcul-sSAAAAAFSSrEE5rjBcW70urZc0dcSJ9i-Q";
								echo recaptcha_get_html($publickey);
							?>
							</p>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<input type="submit" class="btn btn-primary btn-block" name="new" id="newfeedform" value="Register" />
						</div>
					</div>
					</form>

				<hr><center><h5 class="text-danger"><b>Warning!</b> Multiple accounts are forbidden. Violators will be permabanned.</h5></center>
		    	<? require_once("common/footer.php"); ?>
		    </div>
	    </div>
	</body>
</html>