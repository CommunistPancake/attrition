<?php
    require_once("models/config.php");
    require_once("common/db/link_mysql.php");

    $uid = $_GET["uid"];
    require_once("common/uiddata.php");

    // Make sure the uid is a valid and active player #########################################
    if($uid_a[user_id] == null) {
        $result = mysql_query("SELECT active FROM {$dbprefix}users WHERE user_id='$uid' AND active='1' LIMIT 1", $link);
        if (!$result) {
            die('Could not query:' . mysql_error());
        } else {
            $result = mysql_query("SELECT user_id FROM {$dbprefix}users WHERE active='1' ORDER BY RAND() LIMIT 1", $link);
            $uid = mysql_result($result, 0);
            require("common/uiddata.php");
            $errors[] = '<p class="lead">A player or country with that id was not found. A random profile has been chosen.</p>';
        }
    }

    require_once("common/basicfunctions.php");
    require_once("common/warfunctions.php");
    require_once("common/userdata.php");
    $aid = $uid_a[alliance_id];
    require_once("common/alliancedata.php");
    require_once("common/db/pdo.php");
    require_once("common/userfunctions.php");

    // Require again after war functions have completed.
    require("common/uiddata.php");
?>
<html>
    <head>
        <title><? echo stripcslashes(ucwords($uid_a[country_name])) ?> | <? require_once("models/config.php"); echo $websiteName; ?></title>
    </head>

    <body>
        <? require_once("common/navigation.php"); ?>

        <div class="container">
            <div class="well">
                <? require_once("common/alerts.php"); ?>


        <div class="row">

          <div class="col-md-5" align="left">

            <h3>
            <?
              if($uid_a[user_id] == $a_a[founder_id]) {
                echo '<span class="text-warning"><i class="fa fa-star"></i></span> ';
              }
              if($uid_a[donor] == true) {
                echo '<span class="text-success"><i class="fa fa-heart"></i></span> ';
              }
              if($uid_a[banned] == true) {
                echo '<span class="text-danger">BANNED: '.stripcslashes(ucfirst($uid_a[username])).'</a>';
              } elseif($uid_a[custom_title] and $u_a[safe_mode] == 0) {
                echo $uid_a[custom_title].': '.stripcslashes(ucfirst($uid_a[username])).'</a>';
              } else {
                echo getleaderprefix($uid_a[gov_type]).': '.stripcslashes(ucwords($uid_a[username]));
              }
            ?>
            </h3>

          </div>

          <div class="col-md-7" align="right">

            <h3>
            <?php echo getcountryprefix($uid_a[gov_type]) . ' <u><a href="user.php?uid='.$uid_a[user_id].'">'.stripcslashes(ucwords($uid_a[country_name])).'</a></u>'; ?>
            </h3>

          </div>

        </div>
        <?
          if($uid_a[profile_quote]) {
            echo '<hr><blockquote>"'.$uid_a[profile_quote].'"</blockquote>';
          }
        ?>

    <hr>

        <div class="row">
            <div class="col-md-4">

              <!-- Account Info -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><i class="fa fa-info-circle"></i> Account Information</h3>
                </div>
                <div class="panel-body">

                <small>
                  <p class="text-muted">
                  UID: <?php echo ucfirst($uid_a[user_id]); ?>
                  </p>
                  <p class="text-muted">
                  Username: <?php echo stripcslashes(ucwords($uid_a[username])); ?>
                  </p>
                  <p class="text-muted">
                  Country Name: <?php echo stripcslashes(ucwords($uid_a[country_name])); ?>
                  </p>
                  <p class="text-muted">
                  Registration Date: <?php echo date('F, d, o',$uid_a[sign_up_date]); ?>
                  </p>
                  <p class="text-muted">
                  Last Login:
                  <?php
                    // echo date("F, d, o",$uid_a[last_sign_in]);
                  echo date('m/d/y',$uid_a[last_sign_in]).' at '.date('g:i A T',$uid_a[last_sign_in]);
                  ?>
                  </p>
                  <p class="text-muted">
                    Country Age:
                    <?
                      $datetime1 = new DateTime(date('o-m-d ' ,$uid_a[sign_up_date]));
                      $datetime2 = new DateTime();
                      $interval = $datetime1->diff($datetime2);
                      echo $interval->format('%a days');
                    ?>
                  </p>
                </small>

                </div>
              </div>

              <!-- Description -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><i class="fa fa-file-text"></i> <? echo getcountryprefix($uid_a[gov_type]).' <a class="text-danger" href="user.php?uid='.$uid_a[user_id].'"><u>'.ucwords($uid_a[country_name]).'.</u></a>'; ?></h3>
                </div>
                <div class="panel-body">
                  <ul class="media-list">
                    <li class="media">
                      <a class="pull-left">
                        <img class="media-object" style="width: 128px;"
                          <?
                              if($uid_a[custom_leader] and $u_a[safe_mode] == 0) {
                                  echo 'style="width: 100%;" src="'.$uid_a[custom_leader].'">';
                              } else {
                                  echo 'style="width: 100%;" src="'.getleaderfile($uid_a[country_leader]).'">';
                              }
                          ?>
                      </a>
                      <div class="media-body">
                        <h5 class="media-heading">
                      
                        </h5>
                          <?php 
                            if($uid_a[description] == null) {
                              if($uid_a[user_id] == $u_a[user_id]) {
                                echo '<small>Welcome to Attrition. This is the default country description. To change your description visit the <a href="preferences.php">settings</a> page.</small>';
                              } else {
                                echo '<span class="text-muted"><small>This country\'s leader has not set their description yet. <em>The country might be an easy target for an attack.</em></small></span>';
                              }
                            } else {
                              echo '<span class="text-muted"><small>' . stripcslashes($uid_a[description]) . '</small></span>';
                            }
                          ?>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>

              <!-- Awards -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><i class="fa fa-trophy"></i> Decorations</h3>
                </div>
                <div class="panel-body">

                <?php

                    $result = mysql_query("SELECT * FROM {$dbprefix}awards WHERE user_id='$uid_a[user_id]'", $link);
                    if (!$result) {
                        die('Could not query:' . mysql_error());
                    } else {
                        $award_c = mysql_num_rows($result);
                        $award_a = mysql_fetch_array($result, MYSQL_BOTH);
                    }

                  if(!$award_c) {

                    echo '<span class="text-muted"><small>No decorations to display.</small></span>';

                  } else {

                    echo '<hr>';

                    do {

                      echo
                      '<p><b>'.
                      ucfirst($award_a[award_name]).
                      '</b></p>'
                      ;

                      if($award_a[award_reason]) {

                        echo
                        '<p class="text-muted">Awarded for '.
                        stripcslashes($award_a[award_reason]).
                        '</p>'
                        ;

                      }

                      echo
                      '<p class="text-muted"><small>'.
                      date('l, F jS, o',strtotime($award_a[awarded_date])).
                      '.</small></p>'
                      ;

                      echo '<hr>';

                    } while ($award_a = mysql_fetch_assoc($result));

                  }

                ?>
                </small>

                </div>
              </div>

            </div>

          <div class="col-md-8">

            <!-- <div class="col-md-12"> -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-flag"></i> Country Flag</h3>
                    </div>
                    <div class="panel-body">
                        <center>
                        <?
                            if($uid_a[custom_flag] and $u_a[safe_mode] == 0) {
                                echo '<img style="width: 100%;" src="'.$uid_a[custom_flag].'">';
                            } else {
                                echo getflagfile($uid_a[country_flag]);
                            }
                        ?>
                        </center>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-flag"></i> Country Anthem</h3>
                    </div>
                    <div class="panel-body">
                        <?
                            if($uid_a[anthem_url]) {
                                echo '<center>'.getanthem($uid_a[user_id]).'</center>';
                            } else {
                                echo '<span class="text-muted"><small>This country has not set an official anthem.</small></span>';
                            }
                        ?>
                    </div>
                </div>
            <!-- </div> -->

          </div>
        </div>
    <hr>
        <div class="row">
          <div class="col-md-6">
            <!-- Economic -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><i class="fa fa-money"></i> Economic</h3>
                </div>
                <div class="panel-body">

              <table class="table table-hover table-striped">
                <tbody>
                  <tr>
                    <td>Economic System</td>
                    <td>
                      
                    <?php echo ucwords($uid_a[eco_type]);  ?>

                    </td>
                  </tr>
                  <tr>
                    <td>Quality of life</td>
                    <td><?php echo getqol($uid_a[user_id]) ?></td>
                  </tr>
                  <tr>
                    <td>Funds</td>
                    <td><?php 
                          if($uid_a[user_id] == $u_a[user_id]) {
                            echo '$' . $english_format_number = number_format($uid_a[funds]);
                          } elseif ($u_a[debug_mode]) {
                            echo '<span class="text-danger">Classified [$'.$english_format_number = number_format($uid_a[funds]).']</span>';
                          } else { echo '<span class="text-danger">Classified</span>';}
                        ?></td>
                  </tr>
                  <tr>
                    <td>GDP</td>
                    <td>$<?php echo $english_format_number = number_format($uid_a[gdp]); ?></td>
                  </tr>
                  <tr>
                    <td>Growth</td>
                    <td>
                    <?php

                      if($uid_a[growth] >= 0 ) {

                        echo '$' . $english_format_number = number_format($uid_a[growth]);

                      } elseif($uid_a[growth] < 0) {

                        echo '-$' . $english_format_number = str_replace('-', '', number_format($uid_a[growth]));

                      }


                    ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Foreign Investment</td>
                    <td>
                      
                    <?php

                      if($uid_a[foreign_investment] > 0) {

                        echo '$' . $english_format_number = number_format($uid_a[foreign_investment]);

                      } elseif($uid_a[foreign_investment] <= 0) {

                        echo 'None';

                      }

                    ?>

                    </td>
                  </tr>
                    <tr>
                    <td>Oil Production</td>
                    <td>
                      
                    <?php
                    
                      if($uid_a[oil_production] > 0) {

                        echo $uid_a[oil_production] . ' Mbbl';

                      } elseif($uid_a[oil_production] <= 0) {

                        echo 'No Production';

                      } elseif($uid_a[has_prospected] == false) {

                        echo 'Unprospected';

                      }

                    ?>

                    </td>
                  </tr>
                  <tr>
                    <td>Stategic Oil Reserves</td>
                    <td>
                      
                    <?php

                        echo $uid_a[oil_reserves] . ' Mbbl';

                    ?>

                    </td>
                  </tr>
                </tbody>
              </table>
               
                </div>
              </div>
          </div>

          <div class="col-md-6">
            <!-- Domestic -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><i class="fa fa-briefcase"></i> Domestic</h3>
                </div>
                <div class="panel-body">

              <table class="table table-hover table-striped">
                <tbody>

                  <tr>
                    <td>Political System</td>
                    <td><?php echo ucwords($uid_a[gov_type]); ?></td>
                  </tr>
                  <tr>
                    <td>Declaration Date</td>
                    <td><?php echo date("F, d, o",$uid_a[sign_up_date]); ?></td>
                  </tr>
                  <tr>
                    <td>Territory</td>
                    <td><?php echo $english_format_number = number_format($uid_a[territory]) . ' km<sup>2</sup>'; ?></td>
                  </tr>
                  <tr>
                    <td>Population</td>
                    <td><?php echo $english_format_number = number_format($uid_a[population]); ?></td>
                  </tr>
                  <tr>
                    <td>Rebel Threat</td>
                    <td><?php echo getrebelthreat($uid_a[user_id]) ?></td>
                  </tr>
                  <tr>
                    <td>Stability</td>
                    <td><?php echo getstability($uid_a[user_id]) ?></td>
                  </tr>
                </tbody>
              </table>

                </div>
              </div>
          </div>
        </div>
    <hr>
        <div class="row">
          <div class="col-md-6">
            <!-- Foreign -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><i class="fa fa-globe"></i> Foreign</h3>
                </div>
                <div class="panel-body">

              <table class="table table-hover table-striped">
                <tbody>
                  <tr>
                    <td>Alignment</td>
                    <td><?php echo getalignment($uid_a[user_id]) ?></td>
                  </tr>
                  <tr>
                    <td>Region</td>
                    <td><?php echo stripcslashes(ucwords($uid_a[region])); ?></td>
                  </tr>
                  <tr>
                    <td>Alliance</td>
                    <td>
                    <?php 

                      if($uid_a[alliance_id]) {

                        echo '<a href="alliance.php?aid=' . $a_a[alliance_id] . '">' . stripcslashes(ucwords($a_a[alliance_name])) . '</a>';

                      } elseif(!$uid_a[alliance_id]) {

                        echo 'Not Affiliated';

                      }


                    ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Reputation</td>
                    <td><?php echo getreputation($uid_a[user_id]) ?></td>
                  </tr>
                  <tr>
                    <td>Intelligence Agency</td>
                    <td><small>
                    <?
                        if($uid_a[intel_agency] != null) {
                            echo '<b>'.stripcslashes(ucwords($uid_a[intel_agency])).'</b>';
                        } else {
                            echo 'None';
                        }
                    ?>
                    </small></td>
                  </tr>
                </tbody>
              </table>

                </div>
              </div>
          </div>

          <div class="col-md-6">
            <!-- Military -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><i class="fa fa-fighter-jet"></i> Military</h3>
                </div>
                <div class="panel-body">
                  <table class="table table-hover table-striped">
                    <tbody>
                      <tr>
                        <td>Troops</td>
                        <td>
                          <?php echo $english_format_number = number_format($uid_a[troops]); ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Manpower</td>
                        <td>
                          <?php echo getmanpower($uid_a[user_id]); ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Training</td>
                        <td>
                          <?php echo gettraining($uid_a[user_id]); ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Air Force</td>
                        <td>
                          <?php echo getairforce($uid_a[user_id]); ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Chemical Weapons</td>
                        <td>
                          <small>N/A</small>
                        </td>
                      </tr>
                      <tr>
                        <td>Military-Industrial Complex</td>
                        <td>
                          <small>N/A</small>
                        </td>
                      </tr>
                      <tr>
                        <td>Equipment</td>
                        <td>
                          <?php echo getequipment($uid_a[user_id]); ?>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <?php echo getequipmentprogress($uid_a[user_id], $u_a[user_id]); ?>
                </div>
              </div>
          </div>
        </div>
      <hr>
        <div class="row">
          <div class="col-md-12">
            <!-- War -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">War Relations</h3>
                </div>
                <div class="panel-body">
                <?
                    echo getwarinfo($uid_a[user_id]);
                    echo getwaroptions($u_a[user_id], $uid_a[user_id]);

                ?>

                </div>
              </div>

          </div>

            <?
                if($u_a[user_id] != null and $u_a[user_id] != $uid_a[user_id]) {
            ?>
            <div class="col-md-12">
            <!-- Message -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Private Message</h3>
                </div>
                <div class="panel-body">

                  <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
                    <p><textarea class="form-control" placeholder="Use this to send hot and steamy messages to your allies or call your enemies names." name="private_message"></textarea></p>
                    <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#send_pm">
                      Send Message
                    </button>
                    <div class="modal fade" id="send_pm" tabindex="-1" role="dialog" aria-labelledby="send_pm" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="send_pm">Confirm</h4>
                          </div>
                          <div class="modal-body">
                            <input type="submit" class="btn btn-primary btn-block" value="Send Message" name="pm" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>

                </div>
              </div>
            </div>
            <? } ?>

        </div>
        
                <? require_once("common/footer.php"); ?>
            </div>
        </div>
    </body>
</html>